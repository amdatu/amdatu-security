:imagesdir: ./images

//anchor:ch-introduction[]

== Introduction

When developing applications, getting the functional and business requirements
can be a rather intensive and complex matter. Most of the time and energy is
often spent on the business and functional requirements, giving the other non-
functional requirements less or even no attention.

The security aspects of an application are often perceived as complex and
difficult as they impact the software at many places and tend to get "`in the
way`" of developers. As such, security features are often "`bolted`" on top as an
afterthought of the existing functionality.

== Concepts

Amdatu Security provides a solution for authentication and authorisation of
(web-based) applications. Throughout the rest of this document, several terms
and concepts are used that need to be defined in more depth.

Principal::
  An entity in a (computer) system that is granted permissions by which it
  can interact with this system. Examples of a principal are: human users,
  or other systems;

Credentials::
  A set of attributes that are used to identify principals in a system.
  Examples of credentials are: username, password, email addresses, and
  cryptographic keys;

Realm::
  Defines a perimeter for (parts of) a system with certain well-defined
  security policies, such as the permitted principals, how they can
  interact with the system and in what way;

Authentication::
  The process of proofing the identity of a principal in a system, for
  example by means of handing over credentials that uniquely and positively
  identify the principal;

Authorisation::
  The process of defining whether or not principals have access to (parts of)
  a system. Authorisations are often defined at an organisational level;

Access Policy::
  A set of rules that define which principals are granted access to what
  parts of the system. Access policies are often broader than a system
  and defined at organisational level;

Access Control::
  A set of access policies that define the access to a particular system.
  There are various forms of implementing access control, for example,
  role-based in which the role of a principal defines whether or not
  access is granted or attribute-based in which the attributes of the
  principal, the resource and system define whether or not access granted;

Identity Provider::
  A system that provides the identity of a principal, often by means of
  credentials. Examples of identity providers are: social identity
  providers, such as Google and Facebook, or a local database of usernames
  and passwords.

=== The authentication process

Amdatu Security provides configurable means for defining how principals are
authenticated. At a high level, the authentication process in Amdatu Security
is split in two (see also the figure below):

1. the principal first needs to prove its identity using a particular
   identity provider. Multiple identity providers might be configured for
   a realm, but only one is used to provide the identity of a principal.
   An identity provider can be an external system outside the perimeter
   of your system, such as Google or Facebook;
2. once the identity provider has either confirmed the identity of the
   principal, it calls back to Amdatu Security with the identity of the
   principal. Amdatu security then grants the principal with its own
   access token by which it can access the rest of the system.

[plantuml, as_auth_overview, png, align="center"]
....
hide footbox
actor User
participant "Amdatu Security"
collections "Identity Providers"
User -> "Amdatu Security" : login(providerName)
"Amdatu Security" -> "Amdatu Security" : lookup provider(providerName)
"Amdatu Security" -> "Identity Providers" : initiate login(...)
"Identity Providers" --> User : supply credentials
User -> "Identity Providers" : login(credentials)
"Identity Providers" --> "Amdatu Security" : finish login(identity)
"Amdatu Security" -> "Amdatu Security" : lookup principal(identity)
"Amdatu Security" --> User : access token
....

More details on the authentication part of Amdatu Security is given in the chapter on
<<Authentication>>.

//=== The authorisation process
//
//WARNING: TODO
