== Dependencies

The following table provides an overview of the required and optional dependencies for Amdatu Security:

[cols="40%,10%,50%"]
|===
| Bundle
    | Required
    | Description

| OSGi HTTP Whiteboard implementatoin (e.g. Felix HTTP)
    | yes
    | Dynamically picks up the Servlets that represent the JAX-RS resources
| org.apache.felix.dependencymanager
    | yes
    | Apache Felix Dependency Manager is used internally by all Amdatu components
|===
