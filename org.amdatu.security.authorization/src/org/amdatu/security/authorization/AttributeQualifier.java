/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import java.util.Map;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * An AttributeQualifier is used to check if a certain qualification holds for an attribute value.
 *
 * "For example, it can be used to express things like: "Person.age > 18", "Person.address.zipcode startsWith 7311".
 */
@ConsumerType
public interface AttributeQualifier {
    /**
     * @return a selector for the entity that contains the attribute that needs to be checked
     */
    public EntitySelector getEntitySelector();

    /**
     * Is the specified entity qualified, according to this qualifier?
     *
     * @param entity
     *            the entity to check, selected based on {@link #getEntitySelector()}
     * @return true if the specified entity qualifies, false otherwise
     */
    public boolean isQualified(Map<String, Object> properties);
}
