/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization;

import org.osgi.annotation.versioning.ConsumerType;

/**
 * Represents a declarative way to select an entity of particular type.
 */
@ConsumerType
public interface EntitySelector {
    /**
     * @return the type of entity to select
     */
    public String getEntityType();

    /**
     * @return the key under which the entity is intended to be stored and/or retrieved
     */
    public String getKey();

    /**
     * @param obj
     *            the other object
     * @return true if the specified object is any sub-type of EntitySelector that returns the same values from the
     *         methods in this interface, false otherwise
     */
    @Override
    public boolean equals(Object obj);

    @Override
    public int hashCode();
}
