/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.builder;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.amdatu.security.authorization.AttributeQualifier;
import org.amdatu.security.authorization.EntityDescriptor;
import org.amdatu.security.authorization.EntitySelector;

public class EntityDescriptorBuilder extends BuilderBase<EntityDescriptor> {

    public static EntityDescriptorBuilder build() {
        return new EntityDescriptorBuilder();
    }

    public static EntityDescriptor buildTypeDescriptor(Class<?> entityType) {
        EntitySelector selector = EntitySelectorBuilder.build().withType(entityType.getName()).done();
        AttributeQualifier qualifier = AttributeQualifierBuilder.build().withEntitySelector(selector).withQualification((a) -> true).done();
        return EntityDescriptorBuilder.build().withAttributeQualifiers(qualifier).done();
    }

    private EntityDescriptorBuilder() {
        super(() -> new EntityDescriptorImpl());
    }

    public EntityDescriptorBuilder withEntityTypes(String... entityTypes) {
    	return with((EntityDescriptorImpl descriptor) -> Collections.addAll(descriptor.entityTypes, entityTypes));
    }

    public EntityDescriptorBuilder withAttributeQualifiers(AttributeQualifier... attributeQualifiers) {
        return with((EntityDescriptorImpl descriptor) -> Collections.addAll(descriptor.attributeQualifiers, attributeQualifiers));
    }

    @Override
    protected void finalizeInstance() {
        with((EntityDescriptorImpl descriptor) -> descriptor.attributeQualifiers = Collections.unmodifiableSet(descriptor.attributeQualifiers));
    }

    private static final class EntityDescriptorImpl implements EntityDescriptor {
        private Set<AttributeQualifier> attributeQualifiers = new HashSet<>();
        private Set<String> entityTypes = new HashSet<>();

        @Override
        public Set<AttributeQualifier> getAttributeQualifiers() {
            return attributeQualifiers;
        }

		@Override
		public Set<String> getEntityTypes() {
			return entityTypes;
		}

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((entityTypes == null) ? 0 : entityTypes.hashCode());
            result = prime * result + ((attributeQualifiers == null) ? 0 : attributeQualifiers.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }

            EntityDescriptorImpl other = (EntityDescriptorImpl) obj;
            if (entityTypes == null) {
                if (other.entityTypes != null) {
                    return false;
                }
            }
            else if (!entityTypes.equals(other.entityTypes)) {
                return false;
            }

            if (attributeQualifiers == null) {
                if (other.attributeQualifiers != null) {
                    return false;
                }
            }
            else if (!attributeQualifiers.equals(other.attributeQualifiers)) {
                return false;
            }
            return true;
        }

    }
}
