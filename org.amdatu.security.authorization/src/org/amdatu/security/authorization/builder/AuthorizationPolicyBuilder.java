/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.builder;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.amdatu.security.authorization.Action;
import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationRule;
import org.amdatu.security.authorization.EntityDescriptor;

public class AuthorizationPolicyBuilder extends BuilderBase<AuthorizationPolicy> {

    public static AuthorizationPolicyBuilder build() {
        return new AuthorizationPolicyBuilder();
    }

    private AuthorizationPolicyBuilder() {
        super(() -> new AuthorizationPolicyImpl());
    }

    public AuthorizationPolicyBuilder forActions(Action... actions) {
        return with((AuthorizationPolicyImpl policy) -> Collections.addAll(policy.actions, actions));
    }

    public AuthorizationPolicyBuilder forActions(Set<? extends Action> actions) {
        return with((AuthorizationPolicyImpl policy) -> policy.actions.addAll(actions));
    }

    public AuthorizationPolicyBuilder withSubjectDescriptor(EntityDescriptor subjectDescriptor) {
        return with((AuthorizationPolicyImpl policy) -> policy.subjectDescriptor = subjectDescriptor);
    }

    public AuthorizationPolicyBuilder withResourceDescriptor(EntityDescriptor resourceDescriptor) {
        return with((AuthorizationPolicyImpl policy) -> policy.resourceDescriptor = resourceDescriptor);
    }

    public AuthorizationPolicyBuilder withRules(AuthorizationRule... rules) {
        return with((AuthorizationPolicyImpl policy) -> Collections.addAll(policy.rules, rules));
    }

    @Override
    protected void finalizeInstance() {
        with((AuthorizationPolicyImpl policy) -> policy.actions = Collections.unmodifiableSet(policy.actions));
        with((AuthorizationPolicyImpl policy) -> policy.rules = Collections.unmodifiableSet(policy.rules));
    }

    private static class AuthorizationPolicyImpl implements AuthorizationPolicy {
        private Set<Action> actions = new HashSet<>();
        private EntityDescriptor subjectDescriptor = EntityDescriptor.UNDEFINED;
        private EntityDescriptor resourceDescriptor = EntityDescriptor.UNDEFINED;
        private Set<AuthorizationRule> rules = new HashSet<>();

        @Override
        public Set<Action> getActions() {
            return actions;
        }

        @Override
        public EntityDescriptor getSubjectDescriptor() {
            return subjectDescriptor;
        }

        @Override
        public EntityDescriptor getResourceDescriptor() {
            return resourceDescriptor;
        }

        @Override
        public Set<AuthorizationRule> getRules() {
            return rules;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + ((resourceDescriptor == null) ? 0 : resourceDescriptor.hashCode());
            result = prime * result + ((rules == null) ? 0 : rules.hashCode());
            result = prime * result + ((subjectDescriptor == null) ? 0 : subjectDescriptor.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (obj == null) {
                return false;
            }
            if (getClass() != obj.getClass()) {
                return false;
            }
            AuthorizationPolicyImpl other = (AuthorizationPolicyImpl) obj;
            if (resourceDescriptor == null) {
                if (other.resourceDescriptor != null) {
                    return false;
                }
            }
            else if (!resourceDescriptor.equals(other.resourceDescriptor)) {
                return false;
            }
            if (rules == null) {
                if (other.rules != null) {
                    return false;
                }
            }
            else if (!rules.equals(other.rules)) {
                return false;
            }
            if (subjectDescriptor == null) {
                if (other.subjectDescriptor != null) {
                    return false;
                }
            }
            else if (!subjectDescriptor.equals(other.subjectDescriptor)) {
                return false;
            }
            return true;
        }
    }
}
