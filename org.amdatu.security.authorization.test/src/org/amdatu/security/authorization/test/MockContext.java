/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.test;

import java.util.HashMap;
import java.util.Map;

public final class MockContext {
    private static final MockContext INSTANCE = new MockContext();

    public static MockContext get() {
        return INSTANCE;
    }

    public static final String USER_ROLE_KEY = "USER.ROLE";
    public static final String USER_ORGANIZATION_KEY = "USER.ORGANIZATION";
    public static final String USER_ASSOCIATED_PERSON_KEY = "USER.ASSOCIATED_PERSON";

    private Map<Object, Object> m_contents = new HashMap<Object, Object>();

    private MockContext() {
        // prevent external instantiation
    }

    @SuppressWarnings("unchecked")
    public <T> T get(Object key) {
        return (T) m_contents.get(key);
    }

    public void put(Object key, Object value) {
        m_contents.put(key, value);
    }

    public boolean has(Object key) {
        return m_contents.containsKey(key);
    }

    public String getUserUUID() {
        return "dummy";
    }

    public void clear() {
        m_contents.clear();
    }
}
