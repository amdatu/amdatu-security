/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account;

/**
 * Constants used in this API.
 */
public interface AccountConstants {

    String TOPIC_PREFIX = "org/amdatu/security/account";

    /** Topic that reports newly created accounts. */
    String TOPIC_ACCOUNT_CREATED = TOPIC_PREFIX + "/CREATED";
    /** Topic that reports account updates. */
    String TOPIC_ACCOUNT_UPDATED = TOPIC_PREFIX + "/UPDATED";
    /** Topic that reports account removals. */
    String TOPIC_ACCOUNT_REMOVED = TOPIC_PREFIX + "/REMOVED";
    /** Topic that reports report when account credentials are reset. */
    String TOPIC_ACCOUNT_RESET_CREDENTIALS = TOPIC_PREFIX + "/RESET_CREDENTIALS";
    /** Topic that reports whether account owner need to be notified of a change, for example, a signup or credential reset. */
    String TOPIC_NOTIFY_ACCOUNT_OWNER = TOPIC_PREFIX + "/NOTIFY_OWNER";

    /** Event property used in <b>all</b> topics that reports the account identifier. */
    String KEY_ACCOUNT_ID = "id";
    /** Event property used in <b>all</b> topics that reports the <b>current</b> account state. */
    String KEY_ACCOUNT_STATE = "state";
    /** Used in the {@link #TOPIC_NOTIFY_ACCOUNT_OWNER} and denotes the account email used to contact the account owner. */
    String KEY_ACCOUNT_EMAIL = "email";
    /** Used in {@link #TOPIC_ACCOUNT_CREATED} and {@link #TOPIC_ACCOUNT_RESET_CREDENTIALS} and denotes the verify or reset token. */
    String KEY_ACCOUNT_ACCESS_TOKEN = "accessToken";
    /** Used in the {@link #TOPIC_NOTIFY_ACCOUNT_OWNER} and denotes the URL the account owner can call back on to verify its account or change its password. */
    String KEY_NOTIFY_URL = "url";
    /** Used in the {@link #TOPIC_NOTIFY_ACCOUNT_OWNER} and denotes why the notification is being sent. */
    String KEY_ACCOUNT_ACTION = "action";
    /** Event property used in <b>all</b> topics that reports the non-sensitive account credentials. */
    String KEY_ACCOUNT_CREDENTIALS = "credentials";

}
