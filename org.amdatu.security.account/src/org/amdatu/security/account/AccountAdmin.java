/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.account;

import java.util.Map;
import java.util.Optional;

/**
 * Provides a service for managing accounts.
 */
public interface AccountAdmin {

    /**
     * Returns whether or not an account with the given identifier exists.
     *
     * @return <code>true</code> if there is an account with the given account ID, <code>false</code> otherwise.
     */
    boolean accountExists(String accountId);

    /**
     * Creates a new account using the given account credentials.
     * <p>
     * The given credentials should at least contain a valid email address and account identifier property that
     * can be used to uniquely identify each account. For example, the email address could be defined as the
     * account identifier.
     * </p>
     * <p>
     * After an account has been created successfully, an event on the topic
     * {@link AccountConstants#TOPIC_ACCOUNT_CREATED} is posted with information about the created account. Event
     * listeners can use this information, for example, to send the verification token to the account owner using
     * some sort of out-of-band channel, like email.
     * </p>
     *
     * @param credentials the credentials for the account to create, cannot be <code>null</code> and should
     *        provide enough information to uniquely identify different accounts.
     * @return the just created {@link Account} instance, never <code>null</code>.
     * @throws AccountExistsException in case an account already exists with the same account identifier;
     * @throws AccountValidationException in case the update causes the account to be no longer valid;
     * @throws AccountException in case the given credentials did not provide a valid account identifier or when
     *         the verification token failed to generate.
     */
    Account createAccount(Map<String, String> credentials) throws AccountExistsException, AccountValidationException, AccountException;

    /**
     * Returns the account that matches the given account identifier.
     * <p>
     * This method only returns a valid {@link Account} in case there is an account that matches the given identifier.
     * </p>
     *
     * @param accessToken the access token that matches the account token of the account, cannot be <code>null</code>.
     * @return an account if found, never <code>null</code>.
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     * @throws NoSuchAccountException in case there is no account matching the given access token.
     */
    Account getAccountById(String accountId) throws AccountLockedException, NoSuchAccountException;

    /**
     * Returns the account that matches the given access token.
     * <p>
     * This method only returns a valid {@link Account} in case there is an account that matches the given access token.
     * </p>
     *
     * @param accessToken the access token that matches the account token of the account, cannot be <code>null</code>.
     * @return an account if found, never <code>null</code>.
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     * @throws NoSuchAccountException in case there is no account matching the given access token.
     */
    Account getAccountByAccessToken(String accessToken) throws AccountLockedException, NoSuchAccountException;

    /**
     * Returns the account with the given identifier and credentials.
     * <p>
     * This method only returns a valid {@link Account} in case the given credentials match the credentials of the
     * stored account. Only accounts that are not locked, properly validated and not forcibly need a credential
     * reset are returned.
     * </p>
     *
     * @param accountId the account identifier, cannot be <code>null</code>;
     * @param credentials the account credentials, cannot be <code>null</code>.
     * @return an account if found, or an empty optional in case no account exists with the given account ID, or
     *         the given credentials do not match the credentials of the account.
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     * @throws AccountCredentialResetException in case the credentials of the account were reset and need to be
     *         updated before the application can be used normally again.
     * @throws UnverifiedAccountException in case the account still needs to be verified by the user.
     */
    Optional<Account> getAccount(Map<String, String> credentials) throws AccountLockedException, AccountCredentialResetException, UnverifiedAccountException;

    /**
     * Removes the account with the given identifier.
     * <p>
     * After marking the account for a credentials reset, an event on the topic
     * {@link AccountConstants#TOPIC_ACCOUNT_REMOVED} is posted with information about the reset. Event
     * listeners can use this information, for example, to send a confirmation to the account owner using some
     * sort of out-of-band channel, like email.
     * </p>
     *
     * @param accountId the account identifier, cannot be <code>null</code>.
     * @return the removed account.
     * @throws NoSuchAccountException in case there is no account matching the given identifier;
     * @throws AccountException in case the removal of accounts is not supported.
     */
    Account removeAccount(String accountId) throws NoSuchAccountException, AccountException;

    /**
     * Signals that the account with the given identifier needs a credential reset.
     * <p>
     * After marking the account for a credentials reset, an event on the topic
     * {@link AccountConstants#TOPIC_ACCOUNT_RESET_CREDENTIALS} is posted with information about the reset. Event
     * listeners can use this information, for example, to send the reset token to the account owner using some
     * sort of out-of-band channel, like email.
     * </p>
     *
     * @param accountId the account identifier, cannot be <code>null</code>;
     * @param forced <code>true</code> if the credentials <b>must</b> be changed, <code>false</code> if the user
     *        may change the credentials using the generated token or use the current credentials.
     * @return the {@link Account} instance whose credentials are to be reset, never <code>null</code>.
     * @throws NoSuchAccountException in case there is no account matching the given identifier;
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     * @throws AccountException in case the reset token failed to generate.
     */
    Account resetCredentials(String accountId, boolean forced) throws NoSuchAccountException, AccountLockedException, AccountException;

    /**
     * Updates an account, for example, to change its password or any other account related information.
     * <p>
     * This method should be called when the owner of an account wants to modify its account, but has no longer
     * access to its original credentials and therefore requested a credential reset.
     * </p>
     * <p>
     * After marking the account for a credentials reset, an event on the topic
     * {@link AccountConstants#TOPIC_ACCOUNT_UPDATED} is posted with information about the account update. Event
     * listeners can use this information, for example, to send a confirmation about this to the account owner
     * using some sort of out-of-band channel, like email.
     * </p>
     *
     * @param credentials the <b>new</b> credentials to set, cannot be <code>null</code>;
     * @param resetToken the reset token that was generated by {@link #resetCredentials(String, boolean)} for the same account.
     * @return the updated account, never <code>null</code>.
     * @throws NoSuchAccountException in case there is no account matching the given identifier;
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     * @throws AccountCredentialResetException in case the credentials of the account were reset and need to be
     *         updated before the application can be used normally again.
     * @throws AccountValidationException in case the update causes the account to be no longer valid.
     */
    Account updateAccount(Map<String, String> credentials, String resetToken) throws NoSuchAccountException, AccountLockedException, AccountCredentialResetException, AccountValidationException;

    /**
     * Updates an account, for example, to change its password or any other account related information.
     * <p>
     * This method should be called when the owner of an account wants to modify its account.
     * </p>
     * <p>
     * After marking the account for a credentials reset, an event on the topic
     * {@link AccountConstants#TOPIC_ACCOUNT_UPDATED} is posted with information about the account update. Event
     * listeners can use this information, for example, to send a confirmation about this to the account owner
     * using some sort of out-of-band channel, like email.
     * </p>
     *
     * @param oldCredentials the <b>old</b> credentials that <b>must</b> match the credentials of the account, cannot be <code>null</code>;
     * @param newCredentials the <b>new</b> credentials to set, cannot be <code>null</code>.
     * @return the updated account, never <code>null</code>.
     * @throws NoSuchAccountException in case there is no account matching the given identifier;
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     * @throws AccountCredentialResetException in case the credentials of the account were reset and need to be
     *         updated before the application can be used normally again;
     * @throws UnverifiedAccountException in case the account still needs to be verified by the user.
     * @throws AccountValidationException in case the update causes the account to be no longer valid.
     */
    Account updateAccount(Map<String, String> oldCredentials, Map<String, String> newCredentials)
        throws NoSuchAccountException, AccountLockedException, AccountCredentialResetException, UnverifiedAccountException, AccountValidationException;

    /**
     * Completes the account creation by verifying that the account owner is able to access the verification
     * token using some out-of-bands method, like email.
     * <p>
     * After marking the account for a credentials reset, an event on the topic
     * {@link AccountConstants#TOPIC_ACCOUNT_UPDATED} is posted with information about the account update. Event
     * listeners can use this information, for example, to send a confirmation about this to the account owner
     * using some sort of out-of-band channel, like email.
     * </p>
     *
     * @param accountId the account identifier, cannot be <code>null</code>;
     * @param verificationToken the verification token of the account, cannot be <code>null</code>.
     * @return the verified account, never <code>null</code>.
     * @throws NoSuchAccountException in case there is no account matching the given identifier;
     * @throws AccountLockedException in case the account was locked and cannot be accessed in this way;
     */
    Account verifyAccount(String accountId, String verificationToken) throws NoSuchAccountException, AccountLockedException;

}
