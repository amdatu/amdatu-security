/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.impl;

import org.amdatu.security.authorization.EntityProvider;
import org.amdatu.security.authorization.rbac.RoleProvider;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import static org.amdatu.security.authorization.rbac.RoleProvider.ENTITY_TYPE;
import static org.amdatu.security.authorization.rest.impl.AuthorizationRequestInterceptor.REST_APPLICATION_NAME;

public class SubjectRoleEntityProvider implements EntityProvider<Set<String>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(SubjectRoleEntityProvider.class);

    public final Map<String, RoleProvider> roleProviders = new ConcurrentHashMap<>();

    @Override
    public final String getEntityType() {
        return ENTITY_TYPE;
    }

    @Override
    public Collection<String> getRequiredPropertyKeys() {
        return Collections.emptyList();
    }

    @Override
    public Optional<Set<String>> getEntity(Map<String, Object> subjectAttributes) {
        Object applicationName = subjectAttributes.get(REST_APPLICATION_NAME);
        RoleProvider roleProvider = roleProviders.get(applicationName);

        if (roleProvider == null) {
            LOGGER.warn("No RoleProvider available for application: {}", applicationName);
            return Optional.of(Collections.emptySet());
        } else {
            return Optional.ofNullable(roleProvider.getRoles(subjectAttributes));
        }

    }

    void roleProviderAdded(RoleProvider roleProvider) {
        if (roleProviders.putIfAbsent(roleProvider.getApplicationName(), roleProvider) != null) {
            LOGGER.error("Multiple RoleProvider services for application: {}", roleProvider.getApplicationName());
        }
    }

    void roleProviderRemoved(RoleProvider roleProvider) {
        roleProviders.remove(roleProvider.getApplicationName(), roleProvider);
    }
}
