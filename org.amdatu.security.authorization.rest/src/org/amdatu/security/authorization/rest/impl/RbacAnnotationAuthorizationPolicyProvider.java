/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.impl;

import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_TO_GENERIC_STRING;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.security.DenyAll;
import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Path;

import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.InvokeAction;
import org.amdatu.security.authorization.builder.AttributeQualifierBuilder;
import org.amdatu.security.authorization.builder.AuthorizationPolicyBuilder;
import org.amdatu.security.authorization.builder.AuthorizationRuleBuilder;
import org.amdatu.security.authorization.builder.EntityDescriptorBuilder;
import org.amdatu.security.authorization.builder.EntitySelectorBuilder;
import org.amdatu.security.authorization.rbac.HasRolePredicate;
import org.amdatu.security.authorization.rbac.PermitAllPredicate;
import org.amdatu.security.authorization.rbac.RoleProvider;
import org.osgi.framework.ServiceReference;

/**
 * Provides an {@link AuthorizationPolicyProvider} implementation that generates authorization policies to authorize users to
 * access REST endpoints based on annotations.
 * <p>
 * For role based authorization JAX-RS resources can be annotated with the following annotations:
 *
 * <ul>
 * <li>{@link RolesAllowed} - Specifies the list of roles permitted to invoke a method.</li>
 * <li>{@link PermitAll} - Specifies that all authenticated users are permitted to invoke a method.</li>
 * <li>{@link DenyAll} - Specifies that no user is permitted to invoke a method.</li>
 * </ul>
 * <p>
 * When an annotation is present on class level this is treated as a default and is used for all methods, this default can be
 * overridden on a method level by adding one of the annotations.
 */
public class RbacAnnotationAuthorizationPolicyProvider implements AuthorizationPolicyProvider {

    private final ConcurrentHashMap<ServiceReference<?>, Set<AuthorizationPolicy>> m_authorizationPolicies;

    public RbacAnnotationAuthorizationPolicyProvider() {
        m_authorizationPolicies = new ConcurrentHashMap<>();
    }

    @Override
    public Set<AuthorizationPolicy> getPolicies() {
        return m_authorizationPolicies.values().stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
    }

    protected final void serviceAdded(ServiceReference<?> ref, Object service) {
        m_authorizationPolicies.computeIfAbsent(ref, (key) -> {
            if (!service.getClass().isAnnotationPresent(Path.class)) {
                // Currently only JAX-RS resources are supported
                return null;
            }
            Set<AuthorizationPolicy> policies = getPolicies(service);
            if (policies.isEmpty()) {
                return null;
            }
            return policies;
        });
    }

    protected final void serviceRemoved(ServiceReference<?> ref, Object service) {
        m_authorizationPolicies.remove(ref);
    }

    private Set<AuthorizationPolicy> getPolicies(Object service) {
        HashSet<AuthorizationPolicy> policies = new HashSet<>();

        Method[] methods = service.getClass().getMethods();
        for (Method method : methods) {
            if (isObjectMethod(method)) {
                continue;
            }

            AuthorizationPolicy policy = getPolicy(service, method);
            if (policy != null) {
                policies.add(policy);
            }
        }

        return policies;
    }

    private boolean isObjectMethod(Method method) {
        return Object.class.equals(method.getDeclaringClass());
    }

    private AuthorizationPolicy getPolicy(Object service, Method method) {
        Class<?> serviceClass = service.getClass();
        String resourceEntityType = serviceClass.getName();

        if (method.isAnnotationPresent(RolesAllowed.class)) {
            RolesAllowed rolesAllowed = method.getAnnotation(RolesAllowed.class);
            return createRolesAllowedPolicy(resourceEntityType, method, rolesAllowed.value());
        } else if (method.isAnnotationPresent(PermitAll.class)) {
            return createPermitAllPolicy(resourceEntityType, method);
        } else if (serviceClass.isAnnotationPresent(RolesAllowed.class)) {
            RolesAllowed rolesAllowed = serviceClass.getAnnotation(RolesAllowed.class);
            return createRolesAllowedPolicy(resourceEntityType, method, rolesAllowed.value());
        } else if (serviceClass.isAnnotationPresent(PermitAll.class)) {
            return createPermitAllPolicy(resourceEntityType, method);
        }
        return null;
    }

    private static AuthorizationPolicy createRolesAllowedPolicy(String resourceEntityType, Method method, String[] roles) {
        return AuthorizationPolicyBuilder.build()
                .forActions(InvokeAction.INVOKE_ACTION)
                .withResourceDescriptor(EntityDescriptorBuilder.build()
                        .withEntityTypes(resourceEntityType)
                        .withAttributeQualifiers(AttributeQualifierBuilder.build()
                                .withQualification(attrs -> method.toGenericString().equals(attrs.get(INVOKE_METHOD_TO_GENERIC_STRING)))
                                .done())
                        .done())
                .withRules(AuthorizationRuleBuilder.build()
                        .withSubjectEntitySelectors(EntitySelectorBuilder.build()
                                .withKey(RoleProvider.ENTITY_TYPE)
                                .withType(RoleProvider.ENTITY_TYPE)
                                .done())
                        .withAuthorization(HasRolePredicate.withRoles(roles))
                        .done())
                .done();
    }

    private static AuthorizationPolicy createPermitAllPolicy(String resourceEntityType, Method method) {
        return AuthorizationPolicyBuilder.build()
                .forActions(InvokeAction.INVOKE_ACTION)
                .withResourceDescriptor(EntityDescriptorBuilder.build()
                        .withEntityTypes(resourceEntityType)
                        .withAttributeQualifiers(AttributeQualifierBuilder.build()
                                .withQualification(attrs -> method.toGenericString().equals(attrs.get(INVOKE_METHOD_TO_GENERIC_STRING)))
                                .done())
                        .done())
                .withRules(AuthorizationRuleBuilder.build()
                        .withSubjectEntitySelectors(EntitySelectorBuilder.build()
                                .withKey(RoleProvider.ENTITY_TYPE)
                                .withType(RoleProvider.ENTITY_TYPE)
                                .done())
                        .withAuthorization(new PermitAllPredicate())
                        .done())
                .done();
    }

}
