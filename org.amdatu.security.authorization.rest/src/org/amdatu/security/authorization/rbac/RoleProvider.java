/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rbac;

import java.util.Map;
import java.util.Set;


/**
 * The RoleProvider service is a bridge between the application and the Amdatu Security role based policies for rest
 * endpoints. Applications using the role based access policies must provide user to role mapping by registering a
 * {@link RoleProvider} service.
 */
public interface RoleProvider {

    /**
     * The entity type key used for the roles in the subject attributes.
     */
    String ENTITY_TYPE = "org.amdatu.security.authorization.rbac.SubjectRoles";

    /**
     * The name of the application ( {@link org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants#JAX_RS_NAME})
     * for which the roles provided by this {@link RoleProvider} are applicable.
     *
     * @return the name of the aplication
     */
    String getApplicationName();

    /**
     * Get the roles based on the subjectAttributes from the authorization context.
     *
     * @param subjectAttributes the subject attributes from the authorization context.
     * @return Set of roles.
     */
    Set<String> getRoles(Map<String, Object> subjectAttributes);

}
