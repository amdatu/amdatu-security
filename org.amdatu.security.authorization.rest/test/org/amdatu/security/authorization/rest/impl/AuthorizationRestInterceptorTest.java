/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.impl;

import static org.amdatu.security.authorization.InvokeAction.INVOKE_ACTION;
import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_NAME;
import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_TO_GENERIC_STRING;
import static org.amdatu.security.authorization.InvokeAction.INVOKE_METHOD_TO_STRING;
import static org.amdatu.security.authorization.rest.impl.AuthorizationRequestInterceptor.REST_APPLICATION_NAME;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Method;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.ForbiddenException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.RuntimeDelegate;

import org.amdatu.security.authorization.AuthorizationService;
import org.amdatu.security.authorization.BreadAction;
import org.amdatu.security.authorization.annotation.ReadAction;
import org.amdatu.security.authorization.annotation.ResourceAttribute;
import org.amdatu.web.rest.jaxrs.RequestContext;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AuthorizationRestInterceptorTest {

    public static final String TEST_APPLICATION_NAME = "test-application-name";
    @Mock
    private AuthorizationService mockAuthorizationService;

    @InjectMocks
    private AuthorizationRequestInterceptor requestInterceptor;

    public static class TestResource {

        public void test() { }

        public void testWithParams(@ResourceAttribute("string-attr") String string,
            @ResourceAttribute("object-attr") Object object, String stringWithoutResourceAttributeAnnotation) { }

        @ReadAction("test")
        public void testReadAction() { }
    }

    @Test
    public void testInterceptor() throws Exception {

        TestResource testResource = new TestResource();
        Method method = testResource.getClass().getMethod("test");

        Map<String, Object> subjectAttributes = new HashMap<>();
        subjectAttributes.put("test", "test-value");
        subjectAttributes.put(REST_APPLICATION_NAME, TEST_APPLICATION_NAME);

        HttpServletRequest mockRequest = createMockRequest(subjectAttributes);

        Map<String, Object> resourceAttributes = resourceAttributesForMethod(method);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        RequestContext context = new RequestContext(testResource, method, new Object[0], mockRequest, mockResponse, TEST_APPLICATION_NAME);
        try {
            requestInterceptor.intercept(context);
            Assert.fail("Expected UnauthorizedException");
        } catch (ForbiddenException e) {
            // Expected
        }

        Mockito.verify(mockAuthorizationService).isAuthorized("rest-subject", subjectAttributes, INVOKE_ACTION,
            TestResource.class.getName(), Optional.of(resourceAttributes));
        Mockito.verifyNoMoreInteractions(mockAuthorizationService);
    }

    @Test
    public void testInterceptorReadAction() throws Exception {
        TestResource testResource = new TestResource();
        Method method = testResource.getClass().getMethod("testReadAction");

        Map<String, Object> subjectAttributes = new HashMap<>();
        HttpServletRequest mockRequest = createMockRequest(subjectAttributes);
        subjectAttributes.put(REST_APPLICATION_NAME, TEST_APPLICATION_NAME);

        Map<String, Object> resourceAttributes = resourceAttributesForMethod(method);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        RequestContext context = new RequestContext(testResource, method, new Object[0], mockRequest, mockResponse, TEST_APPLICATION_NAME);

        requestInterceptor.intercept(context);

        Mockito.verify(mockAuthorizationService).isAuthorized("rest-subject", subjectAttributes, INVOKE_ACTION,
            TestResource.class.getName(), Optional.of(resourceAttributes));
        Mockito.verify(mockAuthorizationService).checkAuthorization("rest-subject", subjectAttributes, BreadAction.READ,
                "test", Optional.of(resourceAttributes));
        Mockito.verifyNoMoreInteractions(mockAuthorizationService);
    }

    @Test
    public void testCreateResourceAttributeMapWithoutParams() throws Exception {
        TestResource testResource = new TestResource();
        Method method = testResource.getClass().getMethod("test");

        HttpServletRequest mockRequest = createMockRequest(new HashMap<>());

        Map<String, Object> resourceAttributes = resourceAttributesForMethod(method);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        RequestContext context = new RequestContext(testResource, method, new Object[0], mockRequest, mockResponse);

        Map<String, Object> createResourceAttributeMap =
            AuthorizationRequestInterceptor.createResourceAttributeMap(context);
        Assert.assertEquals(resourceAttributes, createResourceAttributeMap);
    }

    @Test
    public void testCreateResourceAttributeMapWithParams() throws Exception {
        TestResource testResource = new TestResource();
        Method method = testResource.getClass().getMethod("testWithParams", String.class, Object.class, String.class);

        HttpServletRequest mockRequest = createMockRequest(new HashMap<>());

        Map<String, Object> resourceAttributes = resourceAttributesForMethod(method);
        HttpServletResponse mockResponse = mock(HttpServletResponse.class);

        String string = "test-string";
        Object object = new Object();
        String stringWithoutResourceAttributeAnnotation = "ignored";
        RequestContext context = new RequestContext(testResource, method, new Object[] {string, object, stringWithoutResourceAttributeAnnotation}, mockRequest, mockResponse);

        resourceAttributes.put("string-attr", string);
        resourceAttributes.put("object-attr", object);

        Map<String, Object> createResourceAttributeMap = AuthorizationRequestInterceptor.createResourceAttributeMap(context);
        Assert.assertEquals(resourceAttributes, createResourceAttributeMap);
    }

    private Map<String, Object> resourceAttributesForMethod(Method method) {
        Map<String, Object> resourceAttributes = new HashMap<>();
        resourceAttributes.put(INVOKE_METHOD_TO_GENERIC_STRING, method.toGenericString());
        resourceAttributes.put(INVOKE_METHOD_TO_STRING, method.toString());
        resourceAttributes.put(INVOKE_METHOD_NAME, method.getName());
        return resourceAttributes;
    }

    private HttpServletRequest createMockRequest(Map<String, Object> attributes) {
        HttpServletRequest mockRequest = mock(HttpServletRequest.class);
        Dictionary<String, Object> attributesDictionary = new Hashtable<>(attributes);
        when(mockRequest.getAttributeNames()).thenReturn(attributesDictionary.keys());
        when(mockRequest.getAttribute(Mockito.anyString()))
            .thenAnswer(invocation -> attributesDictionary.get(invocation.getArguments()[0]));
        return mockRequest;
    }

}
