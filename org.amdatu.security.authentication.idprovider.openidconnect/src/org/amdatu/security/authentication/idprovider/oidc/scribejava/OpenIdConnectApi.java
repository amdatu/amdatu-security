/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import static org.amdatu.security.authentication.idprovider.oidc.scribejava.OpenIdConnectConstants.CLIENT_AUTH_SECRET_BASIC;

import java.io.OutputStream;
import java.util.List;

import com.github.scribejava.core.builder.api.DefaultApi20;
import com.github.scribejava.core.extractors.TokenExtractor;
import com.github.scribejava.core.httpclient.HttpClient;
import com.github.scribejava.core.httpclient.HttpClientConfig;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.model.Verb;
import com.github.scribejava.core.oauth2.clientauthentication.ClientAuthentication;
import com.github.scribejava.core.oauth2.clientauthentication.HttpBasicAuthenticationScheme;
import com.github.scribejava.core.oauth2.clientauthentication.RequestBodyAuthenticationScheme;

public class OpenIdConnectApi extends DefaultApi20 {
    private final OpenIdConnectConfig m_config;

    public OpenIdConnectApi(OpenIdConnectConfig config) {
        this.m_config = config;
    }

    @Override
    public OpenIdConnectService createService(String apiKey, String apiSecret, String callback, String defaultScope,
                                              String responseType, OutputStream debugStream, String userAgent,
                                              HttpClientConfig httpClientConfig, HttpClient httpClient) {
        return new OpenIdConnectService(this, apiKey, apiSecret, callback, defaultScope, responseType, debugStream,
                userAgent, httpClientConfig, httpClient);
    }

    @Override
    public String getAccessTokenEndpoint() {
        return m_config.getTokenEndpoint();
    }

    @Override
    public TokenExtractor<OAuth2AccessToken> getAccessTokenExtractor() {
        return OpenIdConnectJsonTokenExtractor.instance();
    }

    @Override
    public ClientAuthentication getClientAuthentication() {
        List<String> authMethods = m_config.getTokenEndpointAuthMethodsSupported();
        if (authMethods.isEmpty() || authMethods.contains(CLIENT_AUTH_SECRET_BASIC)) {
            // Default to HTTP basic authentication...
            return HttpBasicAuthenticationScheme.instance();
        }
        // All other forms use the request body to convey the credentials...
        return RequestBodyAuthenticationScheme.instance();
    }

    @Override
    public String getRevokeTokenEndpoint() {
        return m_config.getRevocationEndpoint();
    }

    public String getUserInfoEndpoint() {
        return m_config.getUserinfoEndpoint();
    }

    public Verb getUserInfoVerb() {
        return Verb.GET;
    }

    @Override
    protected String getAuthorizationBaseUrl() {
        return m_config.getAuthorizationEndpoint();
    }

}
