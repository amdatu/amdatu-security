/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.JsonNode;
import com.github.scribejava.core.exceptions.OAuthException;
import com.github.scribejava.core.extractors.OAuth2AccessTokenJsonExtractor;
import com.github.scribejava.core.model.OAuth2AccessToken;
import com.github.scribejava.core.utils.OAuthEncoder;

/**
 * additionally parses OpenID id_token
 */
public class OpenIdConnectJsonTokenExtractor extends OAuth2AccessTokenJsonExtractor {
    private static final Pattern ID_TOKEN_REGEX = Pattern.compile("\"id_token\"\\s*:\\s*\"(\\S*?)\"");

    protected OpenIdConnectJsonTokenExtractor() {
        // Nop
    }

    private static class InstanceHolder {
        private static final OpenIdConnectJsonTokenExtractor INSTANCE = new OpenIdConnectJsonTokenExtractor();
    }

    public static OpenIdConnectJsonTokenExtractor instance() {
        return InstanceHolder.INSTANCE;
    }

    @Override
    protected OAuth2AccessToken createToken(String accessToken, String tokenType, Integer expiresIn, String refreshToken, String scope, JsonNode response, String rawResponse) {
        return new OpenIdToken(accessToken, tokenType, expiresIn, refreshToken, scope,
                extractJsonParameter(rawResponse, ID_TOKEN_REGEX, false), rawResponse);
    }

    private static String extractJsonParameter(String response, Pattern regexPattern, boolean required) throws OAuthException {
        Matcher matcher = regexPattern.matcher(response);
        if (matcher.find()) {
            return OAuthEncoder.decode(matcher.group(1));
        } else if (required) {
            throw new OAuthException("Response body is incorrect. Can't extract a '" + regexPattern.pattern() + "' from this: '" + response + "'", (Exception)null);
        } else {
            return null;
        }
    }
}
