/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.oidc.scribejava;

import com.github.scribejava.core.model.OAuth2AccessToken;
import java.util.Objects;

public class OpenIdToken extends OAuth2AccessToken {
    private static final long serialVersionUID = 1L;

    /**
     * Id_token is part of OpenID Connect specification. It can hold user information that you can directly extract
     * without additional request to provider.
     *
     * See http://openid.net/specs/openid-connect-core-1_0.html#id_token-tokenExample and
     * https://bitbucket.org/nimbusds/nimbus-jose-jwt/wiki/Home
     *
     * Here will be encoded and signed id token in JWT format or null, if not defined.
     */
    private final String openIdToken;

    public OpenIdToken(String accessToken, String tokenType, Integer expiresIn, String refreshToken, String scope,
        String openIdToken, String rawResponse) {
        super(accessToken, tokenType, expiresIn, refreshToken, scope, rawResponse);
        this.openIdToken = openIdToken;
    }

    public OpenIdToken(String accessToken, String openIdToken, String rawResponse) {
        this(accessToken, null, null, null, null, openIdToken, rawResponse);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final OpenIdToken other = (OpenIdToken) obj;
        if (!Objects.equals(getAccessToken(), other.getAccessToken())) {
            return false;
        }
        if (!Objects.equals(getTokenType(), other.getTokenType())) {
            return false;
        }
        if (!Objects.equals(getRefreshToken(), other.getRefreshToken())) {
            return false;
        }
        if (!Objects.equals(getScope(), other.getScope())) {
            return false;
        }
        if (!Objects.equals(openIdToken, other.getOpenIdToken())) {
            return false;
        }
        return Objects.equals(getExpiresIn(), other.getExpiresIn());
    }

    public String getOpenIdToken() {
        return openIdToken;
    }

    @Override
    public String getParameter(String parameter) {
        if ("id_token".equals(parameter)) {
            return openIdToken;
        }
        return super.getParameter(parameter);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 37 * hash + Objects.hashCode(getAccessToken());
        hash = 37 * hash + Objects.hashCode(getTokenType());
        hash = 37 * hash + Objects.hashCode(getExpiresIn());
        hash = 37 * hash + Objects.hashCode(getRefreshToken());
        hash = 37 * hash + Objects.hashCode(getScope());
        hash = 37 * hash + Objects.hashCode(openIdToken);
        return hash;
    }

    @Override
    public String toString() {
        return "OpenIdToken{"
            + "access_token=" + getAccessToken()
            + ", token_type=" + getTokenType()
            + ", expires_in=" + getExpiresIn()
            + ", refresh_token=" + getRefreshToken()
            + ", scope=" + getScope()
            + ", open_id_token=" + openIdToken + '}';
    }
}
