/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.util.crypto;

/**
 * Provides a couple of subtle utility methods that can be used in comparing data using a timing-insensitive manner.
 */
public final class Subtle {

    /**
     * Compares two arrays in a time-constant manner.
     *
     * @return <code>true</code> iff both given arrays are equal, <code>false</code> otherwise.
     */
    public static final boolean isEqual(byte[] a, byte[] b) {
        int diff = a.length ^ b.length;
        for (int i = 0; i < a.length && i < b.length; i++) {
            diff |= a[i] ^ b[i];
        }
        return diff == 0;
    }

}
