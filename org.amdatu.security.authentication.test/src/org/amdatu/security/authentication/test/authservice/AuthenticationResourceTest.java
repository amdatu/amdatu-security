/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.test.authservice;

import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_PROVIDER_NAME;
import static org.amdatu.security.authentication.authservice.AuthenticationConstants.REQ_PROVIDER_TYPE;
import static org.amdatu.security.authentication.authservice.AuthenticationInterceptor.LogoutReason.LOGIN_REQUIRED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_LOGIN_REQUIRED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_NAME;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.PROVIDER_TYPE;
import static org.amdatu.security.authentication.test.app.MyAuthService.KNOWN_USER;
import static org.amdatu.security.tokenprovider.TokenConstants.EXPIRATION_TIME;
import static org.amdatu.security.tokenprovider.TokenConstants.NOT_BEFORE;
import static org.amdatu.security.tokenprovider.TokenConstants.SUBJECT;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createComponent;
import static org.amdatu.testing.configurator.TestConfigurator.createFactoryConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.createServiceDependency;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.osgi.framework.Constants.SERVICE_RANKING;
import static org.osgi.service.http.context.ServletContextHelper.REMOTE_USER;

import java.io.IOException;
import java.time.Instant;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.security.authentication.authservice.AuthenticationContext;
import org.amdatu.security.authentication.authservice.AuthenticationHandler;
import org.amdatu.security.authentication.authservice.AuthenticationInterceptor;
import org.amdatu.security.authentication.authservice.PrincipalLookupService;
import org.amdatu.security.authentication.authservice.RejectInfo;
import org.amdatu.security.authentication.idprovider.local.LocalIdCredentialsProvider;
import org.amdatu.security.authentication.test.app.MyAuthService;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.http.TokenUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;


/**
 * Test cases for {@link AuthenticationHandler}.
 */
public class AuthenticationResourceTest {
    // Managed by Felix DM...
    private volatile AuthenticationHandler m_authHandler;
    private volatile TokenProvider m_tokenProvider;
    // Locally managed...
    private volatile MyAuthService m_authService;
    private volatile AuthenticationInterceptor m_authInterceptor;

    private String m_baseURI;

    private static Map<String, String> asMap(String... entries) {
        Map<String, String> result = new HashMap<>();
        for (int i = 0; i < entries.length; i += 2) {
            result.put(entries[i], entries[i + 1]);
        }
        return result;
    }

    @Before
    public void setUp() throws InterruptedException {
        m_authService = spy(new MyAuthService(KNOWN_USER));

        m_authInterceptor = mock(AuthenticationInterceptor.class);

        int port = Integer.getInteger("org.osgi.service.http.port", 8080);
        m_baseURI = String.format("http://localhost:%d/", port);

        Dictionary<String, Object> authInterceptorProps = new Hashtable<>();
        authInterceptorProps.put(SERVICE_RANKING, 1);
        authInterceptorProps.put("my.auth.interceptor", "itest");

        configure(this)
            .add(createFactoryConfiguration("org.amdatu.security.authentication")
                .set("realm", "integration.test",
                    "osgi.http.whiteboard.context.name", "auth.test",
                    "osgi.http.whiteboard.context.path", "/authtest",
                    "idProviderFilter", "(providerType=local)",
                    "authInterceptorSelect", "(my.auth.interceptor=itest)",
                    "cookieName", "amdatu_token",
                    "preLoginURL", m_baseURI + "authtest/index.html",
                    "landingPageURL", m_baseURI + "my/app",
                    "failureURL", m_baseURI + "my/failure"))
            .add(createFactoryConfiguration("org.amdatu.security.authentication.idprovider.local")
                .set("name", "local",
                    "credentialKeys", "email, password",
                    "signingKeyUri", "random:32",
                    "timestepWindow", "2"))
            .add(createComponent()
                .setInterface(PrincipalLookupService.class.getName(), null)
                .setImplementation(m_authService))
            .add(createComponent()
                .setInterface(LocalIdCredentialsProvider.class.getName(), null)
                .setImplementation(m_authService))
            .add(createComponent()
                .setInterface(AuthenticationInterceptor.class.getName(), authInterceptorProps)
                .setImplementation(m_authInterceptor))
            .add(createServiceDependency()
                .setService(AuthenticationHandler.class).setRequired(true))
            .add(createServiceDependency()
                .setService(TokenProvider.class).setRequired(true))
            .apply();

            // FIXME Tests are intermittently failing in the pipelines build, added a little pause before running the actual test
            Thread.sleep(1000);
    }

    @After
    public void tearDown() {
        cleanUp(this);
    }

    @Test
    public void testHandleSessionWithExpiredTokenOk() throws Exception {
        // Create an expired token...
        Map<String, String> tokenAttrs = generateToken(Instant.now().minusSeconds(20), Instant.now().minusSeconds(1));

        String token = m_tokenProvider.generateToken(tokenAttrs);

        // verify that when the token is given to m_authHandler that we should be logged out...
        RejectInfo rejectInfo = callAuthHandler_HandleSecurity(token);
        assertNotNull(rejectInfo);

        assertEquals(CODE_LOGIN_REQUIRED, rejectInfo.getStatusCode());

        verify(m_authInterceptor).onAuthenticationStart(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verify(m_authInterceptor).onLogout(any(AuthenticationContext.class), eq(LOGIN_REQUIRED));
        verify(m_authInterceptor).onAuthenticationEnd(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verifyNoMoreInteractions(m_authInterceptor);
    }

    @Test
    public void testHandleSessionWithNonExpiredTokenOk() throws Exception {
        // Create a non-expired token...
        Map<String, String> tokenAttrs = generateToken(Instant.now().minusSeconds(10), Instant.now().plusSeconds(10));

        String token = m_tokenProvider.generateToken(tokenAttrs);

        // verify that when the token is given to m_authHandler that we should be logged out...
        HttpServletRequest req = createMockServletRequest(token);
        HttpServletResponse resp = mock(HttpServletResponse.class);

        // verify that when the token is given to m_authHandler that we should be logged out...
        Optional<RejectInfo> rejectInfo = m_authHandler.handleSecurity(req, resp);
        assertFalse(rejectInfo.isPresent());

        verify(m_authInterceptor).onAuthenticationStart(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verifyNoMoreInteractions(m_authInterceptor);

        verify(req).setAttribute(eq(TokenUtil.AMDATU_TOKEN_ATTRIBUTE), argThat(s -> {
            Map<String, String> verifyToken = m_tokenProvider.verifyToken(token);
            return tokenAttrs.entrySet().stream()
                    .allMatch(entry -> verifyToken.get(entry.getKey()).equals(entry.getValue()));
        }));
        verify(req).setAttribute(eq(TokenUtil.AMDATU_TOKEN_PROPERTIES_ATTRIBUTE), argThat((ArgumentMatcher<Map<String, String>>) argument -> {
                    return tokenAttrs.entrySet().stream()
                            .allMatch(entry -> argument.get(entry.getKey()).equals(entry.getValue()));
                }));

        verify(req).setAttribute(eq("invalidTokenReason"), ArgumentMatchers.isNull());
        verify(req).getAttribute(TokenUtil.AMDATU_TOKEN_PROPERTIES_ATTRIBUTE);
        verify(req).getAttribute("invalidTokenReason");


        verify(req).setAttribute(eq(REQ_PROVIDER_TYPE), eq("itest"));
        verify(req).setAttribute(eq(REQ_PROVIDER_NAME), eq("itest"));
        verify(req).setAttribute(eq(REMOTE_USER), eq(KNOWN_USER));

        verify(req).getCookies();

        verifyNoMoreInteractions(req);
    }

    @Test
    public void testHandleSessionWithoutTokenOk() throws Exception {
        RejectInfo rejectInfo = callAuthHandler_HandleSecurity(null);
        assertNotNull(rejectInfo);

        assertEquals(CODE_LOGIN_REQUIRED, rejectInfo.getStatusCode());

        verify(m_authInterceptor).onAuthenticationStart(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verify(m_authInterceptor).onLogout(any(AuthenticationContext.class), eq(LOGIN_REQUIRED));
        verify(m_authInterceptor).onAuthenticationEnd(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verifyNoMoreInteractions(m_authInterceptor);
    }

    private RejectInfo callAuthHandler_HandleSecurity(String token) throws IOException {
        HttpServletRequest req = createMockServletRequest(token);
        HttpServletResponse resp = mock(HttpServletResponse.class);

        // verify that when the token is given to m_authHandler that we should be logged out...
        Optional<RejectInfo> rejectInfo = m_authHandler.handleSecurity(req, resp);
        return rejectInfo.orElse(null);
    }

    @Test
    public void testCallbacksAreCalledOnAuthenticationRejected() throws Exception {
        // we generate an expired token
        Map<String, String> tokenAttrs = generateToken(Instant.now().minusSeconds(20), Instant.now().minusSeconds(10));

        String expiredToken = m_tokenProvider.generateToken(tokenAttrs);

        RejectInfo rejectInfo = callAuthHandler_HandleSecurity(expiredToken);
        assertNotNull(rejectInfo);

        verify(m_authInterceptor).onAuthenticationStart(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verify(m_authInterceptor).onLogout(any(AuthenticationContext.class), eq(LOGIN_REQUIRED));
        verify(m_authInterceptor).onAuthenticationEnd(any(HttpServletRequest.class), any(HttpServletResponse.class));
        verifyNoMoreInteractions(m_authInterceptor);
    }

    private HttpServletRequest createMockServletRequest(String token) {
        final Map<String, Object> attrs = new HashMap<>();

        HttpServletRequest req = mock(HttpServletRequest.class);
        doAnswer(mock -> {
            attrs.put(mock.getArguments()[0].toString(), mock.getArguments()[1]);
            return null;
        }).when(req).setAttribute(anyString(), any());

        when(req.getAttribute(anyString())).thenAnswer(mock -> {
            return attrs.get(mock.getArguments()[0]);
        });
        if (token != null) {
            when(req.getCookies()).thenReturn(new Cookie[] { new Cookie("amdatu_token", token) });
        }

        return req;
    }

    private Map<String, String> generateToken(Instant notBefore, Instant expiration) {
        String nbf = Long.toString(notBefore.getEpochSecond());
        String exp = Long.toString(expiration.getEpochSecond());

        Map<String, String> attrs = asMap(
                SUBJECT, KNOWN_USER,
                PROVIDER_TYPE, "itest",
                PROVIDER_NAME, "itest",
                NOT_BEFORE, nbf,
                EXPIRATION_TIME, exp);

        return attrs;
    }
}
