/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.test.app;

import java.util.concurrent.CountDownLatch;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Test REST resource.
 */
@Path("/my")
public class MyLocalApp {
    public final CountDownLatch m_appCalled = new CountDownLatch(1);
    public final CountDownLatch m_failureCalled = new CountDownLatch(1);

    @GET
    @Path("/app")
    public Response securedResource() {
        m_appCalled.countDown();
        return Response.ok("secure resource").build();
    }

    @GET
    @Path("/failure")
    public Response failurePage() {
        m_failureCalled.countDown();
        return Response.ok("failed").build();
    }
}
