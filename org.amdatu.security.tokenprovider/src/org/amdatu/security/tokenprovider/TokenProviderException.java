/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider;

/**
 * Exception indicating an internal error that occurred during generating or verifying tokens.
 */
public class TokenProviderException extends RuntimeException {
    // The serial version UID of this exception class
    private static final long serialVersionUID = 1008471221110336339L;

    /**
     * Constructs a new token provider exception.
     * 
     * @param msg
     *        An error message providing more information about the cause of the error.
     */
    public TokenProviderException(String msg) {
        super(msg);
    }

    /**
     * Constructs a new token provider exception.
     * 
     * @param t
     *        An exception providing more information about the cause of the error.
     */
    public TokenProviderException(Throwable t) {
        super(t);
    }

    /**
     * Constructs a new token provider exception.
     * 
     * @param msg
     *        An error message providing more information about the cause of the error;
     * @param t
     *        An exception providing more information about the cause of the error.
     */
    public TokenProviderException(String msg, Throwable t) {
        super(msg, t);
    }
}
