/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.http;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public final class TokenUtil {
    /**
     * The HTTP header used to transport token information.
     */
    public static final String AUTHORIZATION_HEADER = "Authorization";
    /**
     * Name of the attribute of the {@link HttpServletRequest} to store the token <em>after</em> it has been validated by the {@link TokenFilter}.
     */
    public static final String AMDATU_TOKEN_ATTRIBUTE = "Amdatu.Token";
    /**
     * Name of the attribute of the {@link HttpServletRequest} to store the token properties <em>after</em> the token has been validated by the {@link TokenFilter}.
     */
    public static final String AMDATU_TOKEN_PROPERTIES_ATTRIBUTE = "Amdatu.Token.Properties";
    /**
     * Name of the cookie that stores an Amdatu token.
     */
    public static final String AMDATU_TOKEN_COOKIE_NAME = "amdatu_token";

    /**
     * Name of the authorization header when a token is passed in the "Authorization" header of a HTTP request.
     */
    private static final String AUTHORIZATION_SCHEME_AMDATU = "Amdatu";
    /**
     *
     */
    private static final String X_FORWARDED_PROTO_HEADER = "X-Forwarded-Proto";

    /**
     * Creates a new {@link Cookie} instance that is preconfigured for the given name and value.
     *
     * @param request the HTTP request to use to create the cookie, cannot be <code>null</code>;
     * @param cookieName the name of the cookie to return, cannot be <code>null</code>;
     * @param cookieValue the value of the cookie to return, cannot be <code>null</code>.
     * @return a new {@link Cookie} instance, never <code>null</code>.
     */
    public static Cookie createCookie(HttpServletRequest request, String cookieName, String cookieValue) {
        Cookie cookie = new Cookie(cookieName, cookieValue);

        cookie.setDomain(request.getServerName());
        // Force the browser to only send the cookie back via HTTPS...
        cookie.setSecure(request.isSecure() || "https".equals(request.getHeader(X_FORWARDED_PROTO_HEADER)));
        cookie.setHttpOnly(true);

        return cookie;
    }

    /**
     * Obtains the cookies with a given name.
     * <p>
     * Be aware that it is possible to have multiple cookies with the same name in a single
     * request. For example, they could have different paths.
     * </p>
     *
     * @param request the HTTP request to obtain the token from, cannot be <code>null</code>;
     * @param cookieName the name of the cookie(s) to return, cannot be <code>null</code>;
     * @return a list of cookies with the matching names, never <code>null</code> (but the list can
     *         be empty).
     */
    public static List<Cookie> getCookies(HttpServletRequest request, String cookieName) {
        List<Cookie> cookies = new ArrayList<>(1);
        Cookie[] cookiesArr = request.getCookies();
        if (cookiesArr != null) {
            for (Cookie cookie : cookiesArr) {
                if (cookieName.equals(cookie.getName())) {
                    cookies.add(cookie);
                }
            }
        }
        return cookies;
    }

    /**
     * Returns the authorization token from the cookies provided by the given HTTP request.
     * <p>
     * This method returns the value of the <em>reassembled</em> cookie values whose cookie name matches the given
     * cookie pattern. The cookieName may be postfixed with an underscore followed by a digit. This postfix will be
     * stripped from the cookie name, afterwards the cookie values are ordered by the postfix digit and concatenated
     * to form the token.
     * </p>
     *
     * @param request the HTTP request to obtain the token from, cannot be <code>null</code>;
     * @param cookieName the name of the cookies to return the value (token) for, cannot be <code>null</code>.
     * @return the value (presumably the token) of the requested cookie.
     */
    public static String getTokenFromCookie(HttpServletRequest request, String cookieName) {
        final Pattern cookieNamePattern = Pattern.compile(Pattern.quote(cookieName) + "(?:_(\\d+))?");

        final TreeMap<Integer, List<Cookie>> chunkedTokenCookies = Optional.ofNullable(request.getCookies())
                .map(Arrays::asList)
                .orElse(Collections.emptyList())
                .stream()
                .filter(Objects::nonNull)
                .filter(cookie -> cookieNamePattern.matcher(cookie.getName()).matches())
                .collect(Collectors.groupingBy(cookie -> {
                    Matcher cookieMatcher = cookieNamePattern.matcher(cookie.getName());
                    return cookieMatcher.matches() && cookieMatcher.group(1) != null ? Integer.parseInt(cookieMatcher.group(1)) : 0;
                }, TreeMap::new, Collectors.toList()));

        final String token = chunkedTokenCookies.keySet().stream()
                .flatMap(key -> chunkedTokenCookies.get(key).stream()
                        .map(Cookie::getValue)
                )
                .collect(Collectors.joining());
        return "".equals(token) ? null : token;
    }

    /**
     * Returns the authorization token from a given HTTP request if present.
     * <p>
     * This method uses the <tt>Authorization</tt> header from the given request and checks whether
     * it starts with the expected given scheme. If so, the part after the scheme is assumed to be
     * the token and returned by this method.
     * </p>
     *
     * @param request the HTTP request to obtain the token from, cannot be <code>null</code>;
     * @param authSchemeName the authentication scheme to expect in the authentication header, cannot be <code>null</code>.
     * @return the token from the authorization header, if present with the correct authentication scheme.
     */
    public static String getTokenFromHeader(HttpServletRequest request, String authSchemeName) {
        String authHeader = request.getHeader(AUTHORIZATION_HEADER);
        String prefix = authSchemeName.concat(" ");
        if (authHeader != null && authHeader.startsWith(prefix)) {
            return authHeader.substring(prefix.length());
        }
        return null;
    }

    /**
     * Returns the request token associated with the specified request or null
     * of none is associated.
     *
     * @param request the HTTP request to obtain the token from, cannot be <code>null</code>.
     * @return the request token associated with the specified request
     */
    public static String getTokenFromRequest(HttpServletRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("Request cannot be null!");
        }

        // Use case 1: The token is sent along in a cookie with the request.
        // The cookie is sent automatically when a request is sent directly from the end user's browser to the Amdatu server.
        String token = getTokenFromCookie(request, AMDATU_TOKEN_COOKIE_NAME);
        if (token == null) {
            // Use case 2: When requests are not sent from a browser (for example proxied calls)
            // the token can be sent in the Authorization header (like Basic and Digest HTTP authentication).
            token = getTokenFromHeader(request, AUTHORIZATION_SCHEME_AMDATU);
        }
        return token;
    }

    /**
     * Invalidates the cookies with the given cookie name by setting its maximum age to zero.
     *
     * @param request the HTTP request to obtain the cookies from, cannot be <code>null</code>;
     * @param response the HTTP response to add the cookies to, cannot be <code>null</code>;
     * @param cookieNames the name(s) of the cookie(s) to invalidate, cannot be <code>null</code> and at least one cookie name should be given.
     */
    public static void invalidateCookies(HttpServletRequest request, HttpServletResponse response, String... cookieNames) {
        for (String cookieName : cookieNames) {
            List<Cookie> cookies = getCookies(request, cookieName);
            for (Cookie cookie : cookies) {
                cookie.setMaxAge(0);

                response.addCookie(cookie);
            }
        }
    }

}
