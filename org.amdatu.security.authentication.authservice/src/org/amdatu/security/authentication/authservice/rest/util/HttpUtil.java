/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice.rest.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Convenience methods for working with {@link HttpServletRequest}s.
 */
public class HttpUtil {
    private static final String X_REQUESTED_WITH = "X-Requested-With";
    private static final String XML_HTTP_REQUEST = "XMLHttpRequest";

    /**
     * Determines whether or not the given request is originating from an AJAX/XmlHttpRequest call.
     *
     * @param req the servlet request, cannot be <code>null</code>.
     * @return <code>true</code> if the given servlet request is an XML HTTP request, <code>false</code> otherwise.
     */
    public static boolean isXmlHttpRequest(HttpServletRequest req) {
        return XML_HTTP_REQUEST.equals(req.getHeader(X_REQUESTED_WITH));
    }

	public static void addCookie(HttpServletResponse response, Cookie cookie, String sameSite) {

		int length = cookie.getValue() == null ? 0 : cookie.getValue().length();
		StringBuilder sb = new StringBuilder(64 + length);

		sb.append(cookie.getName());
		sb.append('=');
		sb.append(cookie.getValue());

		appendTocookie(sb, "domain", cookie.getDomain());
		appendTocookie(sb, "path", cookie.getPath());
		if (!sameSite.equals("")) {
			appendTocookie(sb, "SameSite", sameSite);
		}

		if (cookie.getSecure()) {
			sb.append("; secure");
		}
		if (cookie.isHttpOnly()) {
			sb.append("; HttpOnly");
		}
		if (cookie.getMaxAge() >= 0) {
			appendTocookie(sb, "Expires", getExpires(cookie.getMaxAge()));
		}

		response.addHeader("Set-Cookie", sb.toString());
	}

	private static String getExpires(int maxAge) {
		if (maxAge < 0) {
			return "";
		}
		Calendar expireDate = Calendar.getInstance();
		expireDate.setTime(new Date());
		expireDate.add(Calendar.SECOND, maxAge);
		SimpleDateFormat expiresDateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");
		expiresDateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
		return expiresDateFormat.format(expireDate.getTime());
	}

	private static void appendTocookie(StringBuilder cookie, String key, String value) {
		if (key == null || value == null || key.trim().equals("") || value.trim().equals("")) {
			return;
		}

		cookie.append("; ");
		cookie.append(key);
		cookie.append('=');
		cookie.append(value);
	}

}
