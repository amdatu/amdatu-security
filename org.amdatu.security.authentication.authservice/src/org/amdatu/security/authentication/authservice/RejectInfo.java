/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.authservice;

import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.servlet.http.HttpServletResponse;

/**
 * Provides information about why an {@link AuthenticationHandler} rejected a particular request.
 */
public class RejectInfo {
    private final String m_statusCode;
    private final int m_httpStatusCode;
    private final Throwable m_cause;
    private final URI m_failureURL;
    private final Map<String, List<String>> m_headers;

    /**
     * Creates a new {@link RejectInfo} instance.
     */
    public RejectInfo(String statusCode, URI failureURL) {
        this(statusCode, SC_FORBIDDEN, failureURL, null);
    }

    /**
     * Creates a new {@link RejectInfo} instance.
     */
    public RejectInfo(String statusCode, int httpStatusCode, URI failureURL) {
        this(statusCode, httpStatusCode, failureURL, null);
    }

    /**
     * Creates a new {@link RejectInfo} instance.
     */
    public RejectInfo(String statusCode, URI failureURL, Throwable cause) {
        this(statusCode, SC_FORBIDDEN, failureURL, cause);
    }

    /**
     * Creates a new {@link RejectInfo} instance.
     */
    public RejectInfo(String statusCode, int httpStatusCode, URI failureURL, Throwable cause) {
        m_statusCode = statusCode;
        m_httpStatusCode = httpStatusCode;
        m_failureURL = failureURL;
        m_cause = cause;
        m_headers = new HashMap<>();
    }

    public static Optional<RejectInfo> create(String statusCode, int httpStatusCode, URI failureURL) {
        return create(statusCode, httpStatusCode, failureURL, null);
    }

    public static Optional<RejectInfo> create(String statusCode, int httpStatusCode, URI failureURL, Throwable cause) {
        return Optional.of(new RejectInfo(statusCode, httpStatusCode, failureURL, cause));
    }

    public static Optional<RejectInfo> create(String statusCode, URI failureURL) {
        return create(statusCode, SC_FORBIDDEN, failureURL);
    }

    public static Optional<RejectInfo> create(String statusCode, URI failureURL, Throwable cause) {
        return create(statusCode, SC_FORBIDDEN, failureURL, cause);
    }

    public static Optional<RejectInfo> none() {
        return Optional.empty();
    }

    /**
     * Adds an header that should be send with a servlet response.
     *
     * @param name the name of the header;
     * @param value the value of the header.
     */
    public void addHeader(String name, String value) {
        m_headers.computeIfAbsent(name, ignored -> new ArrayList<>()).add(value);
    }

    /**
     * Applies this {@link RejectInfo} to the given {@link HttpServletResponse} and
     * redirects the client to the failure URL if present.
     *
     * @param resp the servlet response to apply the reject information to, cannot be <code>null</code>.
     */
    public void applyTo(HttpServletResponse resp) throws IOException {
        applyTo(resp, true /* redirect */);
    }

    /**
     * Applies this {@link RejectInfo} to the given {@link HttpServletResponse}.
     *
     * @param resp the servlet response to apply the reject information to, cannot be <code>null</code>;
     * @param redirect <code>true</code> to redirect if a redirect URL is present.
     */
    public void applyTo(HttpServletResponse resp, boolean redirect) throws IOException {
        m_headers.entrySet().stream().forEach(outer -> {
            outer.getValue().stream().forEach(value -> {
                resp.setHeader(outer.getKey(), value);
            });
        });

        resp.setStatus(m_httpStatusCode);
        resp.setHeader("X-AuthFailure-Code", m_statusCode);

        if (m_cause != null) {
            resp.setHeader("X-AuthFailure-Cause", m_cause.getMessage());
        }

        if (redirect && m_failureURL != null) {
            resp.sendRedirect(m_failureURL.toASCIIString());
        }
    }

    /**
     * @return a {@link Throwable} that indicates the cause of the rejection, can be <code>null</code>.
     */
    public Throwable getCause() {
        return m_cause;
    }

    /**
     * @return a URI that can be used to redirect to a proper landing page for the user.
     */
    public URI getFailureURL() {
        return m_failureURL;
    }

    /**
     * @return the (optional) response headers to send back, cannot be <code>null</code>.
     */
    public Map<String, List<String>> getHeaders() {
        Map<String, List<String>> result = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : m_headers.entrySet()) {
            result.put(entry.getKey(), new ArrayList<>(entry.getValue()));
        }
        return result;
    }

    /**
     * @return the HTTP status code that reflects the cause of the failure in HTTP semantics.
     */
    public int getHttpStatusCode() {
        return m_httpStatusCode;
    }

    /**
     * @return the status code that indicates the cause of failure, should not be <code>null</code>.
     */
    public String getStatusCode() {
        return m_statusCode;
    }

}
