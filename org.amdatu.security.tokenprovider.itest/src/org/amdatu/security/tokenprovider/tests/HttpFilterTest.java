/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.tokenprovider.tests;

import static javax.servlet.http.HttpServletResponse.SC_INTERNAL_SERVER_ERROR;
import static javax.servlet.http.HttpServletResponse.SC_NOT_FOUND;
import static javax.servlet.http.HttpServletResponse.SC_OK;
import static org.amdatu.security.tokenprovider.http.TokenUtil.AMDATU_TOKEN_ATTRIBUTE;
import static org.amdatu.security.tokenprovider.tests.TestUtils.asDictionary;
import static org.amdatu.testing.configurator.TestConfigurator.cleanUp;
import static org.amdatu.testing.configurator.TestConfigurator.configure;
import static org.amdatu.testing.configurator.TestConfigurator.createConfiguration;
import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.amdatu.web.testing.http.HttpTestConfigurator.*;
import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.SortedMap;
import java.util.TreeMap;

import javax.servlet.Filter;
import javax.servlet.Servlet;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.amdatu.security.tokenprovider.TokenConstants;
import org.amdatu.security.tokenprovider.TokenProvider;
import org.amdatu.security.tokenprovider.http.TokenFilter;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * @author <a href="mailto:amdatu-developers@amdatu.org">Amdatu Project Team</a>
 */
public class HttpFilterTest {

    static class MyServlet extends HttpServlet {
        private static final long serialVersionUID = 1L;

        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            String token = (String) req.getAttribute(AMDATU_TOKEN_ATTRIBUTE);

            if ("/login".equals(req.getPathInfo())) {
                resp.setStatus(token == null ? SC_OK : SC_INTERNAL_SERVER_ERROR);
            }
            else if ("/protected".equals(req.getPathInfo())) {
                resp.setStatus(token == null ? SC_INTERNAL_SERVER_ERROR : SC_OK);
            }
            else {
                resp.setStatus(SC_NOT_FOUND);
            }
        }
    }

    private static final String JWT_PID = "org.amdatu.security.tokenprovider.jwt";

    private volatile TokenProvider m_tokenProvider;

    private String m_baseURI;

    @Before
    public void setUp() throws Exception {
        int port = Integer.getInteger("org.osgi.service.http.port", 8080);
        m_baseURI = String.format("http://localhost:%d/", port);

        configure(this)
            .add(createConfiguration(JWT_PID)
                .setSynchronousDelivery(true)
                .set("algorithm", "HS256")
                .set("issuer", "test token provider")
                .set("audience", "test token audience")
                .set("validityPeriod", 2 /* seconds */)
                .set("keyUri", "random:/"))
            .add(createServiceDependency()
                .setService(TokenProvider.class, "(type=jwt)")
                .setRequired(true))
            .add(createComponent()
                .setInterface(Servlet.class.getName(), asDictionary("osgi.http.whiteboard.servlet.pattern", "/*"))
                .setImplementation(new MyServlet()))
            .add(createComponent()
                .setInterface(Filter.class.getName(), asDictionary("osgi.http.whiteboard.filter.regex", "/protected.*", "filter.init.authSchemeName", "Test", "filter.init.cookieName", "mycookie"))
                .setImplementation(TokenFilter.class)
                .add(createServiceDependency().setService(TokenProvider.class).setRequired(true)))
            .add(createWaitForHttpEndpoints(m_baseURI + "login"))
            .apply();
    }

    @Test
    public void testAccessProtectedResourceWithoutTokenFail() throws Exception {
        URL url = new URL(m_baseURI + "protected");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            assertEquals(403, conn.getResponseCode());
        }
        finally {
            conn.disconnect();
        }
    }

    @Test
    public void testAccessProtectedResourceWithHeaderTokenOk() throws Exception {
        String token = generateToken();

        URL url = new URL(m_baseURI + "protected");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.addRequestProperty("Authorization", "Test " + token);

        try {
            assertEquals(200, conn.getResponseCode());
        }
        finally {
            conn.disconnect();
        }
    }

    @Test
    public void testAccessProtectedResourceWithCookieTokenOk() throws Exception {
        String token = generateToken();

        URL url = new URL(m_baseURI + "protected");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.addRequestProperty("Cookie", "mycookie=" + token);
        try {
            assertEquals(200, conn.getResponseCode());
        }
        finally {
            conn.disconnect();
        }
    }

    @Test
    public void testAccessUnprotectedResourceWithoutTokenOk() throws Exception {
        URL url = new URL(m_baseURI + "login");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        try {
            assertEquals(200, conn.getResponseCode());
        }
        finally {
            conn.disconnect();
        }
    }

    @Test
    public void testAccessUnprotectedResourceWithTokenHeaderOk() throws Exception {
        String token = generateToken();

        URL url = new URL(m_baseURI + "login");

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.addRequestProperty("Authorization", "Test " + token);
        try {
            assertEquals(200, conn.getResponseCode());
        }
        finally {
            conn.disconnect();
        }
    }

    @After
    public void cleanup() {
        cleanUp(this);
    }

    private String generateToken() {
        SortedMap<String, String> attributes = new TreeMap<>();
        attributes.put(TokenConstants.SUBJECT, "testuser");
        attributes.put("someotherkey", "somevalue");

        return m_tokenProvider.generateToken(attributes);
    }
}
