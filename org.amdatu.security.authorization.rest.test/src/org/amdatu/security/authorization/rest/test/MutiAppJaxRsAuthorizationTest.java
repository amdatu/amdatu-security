/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.test;

import com.github.kevinsawicki.http.HttpRequest;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.rbac.RoleProvider;
import org.amdatu.testing.configurator.TestConfiguration;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.testing.http.HttpTestConfigurator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.Properties;

import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MutiAppJaxRsAuthorizationTest {

    private static final String TEST_SERVLET_CONTEXT_NAME = "org.amdatu.security.authorization.rest.test";
    private static final String TEST_JAX_RS_APP_ONE_NAME = "org.amdatu.security.authorization.rest.test-one";
    private static final String TEST_JAX_RS_APP_TWO_NAME = "org.amdatu.security.authorization.rest.test-two";
    private static final String TEST_JAX_RS_APP_THREE_NAME = "org.amdatu.security.authorization.rest.test-three";

    @RolesAllowed("user")
    @Path("test")
    public static class TestResource {

        @GET
        public String test() {
            return "hello";
        }

        @GET
        @Path("/all")
        @PermitAll
        public String ping() {
            return "hello";
        }

    }

    /**
     * ServletContextHelper implementation that put's the value of <code>Test-User-Id</code> in the
     * {@link ServletContextHelper#REMOTE_USER} attribute of the {@link HttpServletRequest}.
     *
     * Only for testing
     */
    private static final class TestServletContextHelper extends ServletContextHelper {
        @Override
        public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) {
            String testUserId = request.getHeader("Test-User-Id");
            if (testUserId != null) {
                request.setAttribute(ServletContextHelper.REMOTE_USER, testUserId);
            }
            return true;
        }
    }

    @Mock
    private RoleProvider m_appOneRoleProvider;

    @Mock
    private RoleProvider m_appTwoRoleProvider;

    @Mock
    private AuthorizationPolicyProvider m_mockPolicyProvider;

    @Before
    public void before() {
        TestConfiguration testConfiguration = configure(this);

        when(m_appOneRoleProvider.getApplicationName()).thenReturn(TEST_JAX_RS_APP_ONE_NAME);
        when(m_appTwoRoleProvider.getApplicationName()).thenReturn(TEST_JAX_RS_APP_TWO_NAME);

        Properties testServletContextProperties = new Properties();
        testServletContextProperties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, "/");
        testServletContextProperties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, TEST_SERVLET_CONTEXT_NAME);


        configureApp(testConfiguration, TEST_JAX_RS_APP_ONE_NAME, "/one");
        configureApp(testConfiguration, TEST_JAX_RS_APP_TWO_NAME, "/two");
        configureApp(testConfiguration, TEST_JAX_RS_APP_THREE_NAME, "/three");

        testConfiguration
                .add(createComponent().setInterface(ServletContextHelper.class.getName(), testServletContextProperties).setImplementation(new TestServletContextHelper()))
                .add(createComponent().setInterface(RoleProvider.class.getName(), null).setImplementation(m_appOneRoleProvider))
                .add(createComponent().setInterface(RoleProvider.class.getName(), null).setImplementation(m_appTwoRoleProvider))
                .add(createComponent().setInterface(AuthorizationPolicyProvider.class.getName(), null).setImplementation(m_mockPolicyProvider))
                .add(HttpTestConfigurator.createWaitForHttpEndpoints("http://localhost:8080/one/test/all"))
                .apply();
    }

    private void configureApp(TestConfiguration testConfiguration, String appName, String appBase) {
        Properties jaxRsApplicationProps = new Properties();
        jaxRsApplicationProps.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE, appBase);
        jaxRsApplicationProps.put(JaxrsWhiteboardConstants.JAX_RS_NAME, appName);
        jaxRsApplicationProps.put(AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT, TEST_SERVLET_CONTEXT_NAME);
        jaxRsApplicationProps.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION_SELECT, String.format("(%s=%s)", JaxrsWhiteboardConstants.JAX_RS_NAME, "amdatu-authorization"));
        jaxRsApplicationProps.put("amdatu-authorization", true);

        Properties testResourceProps = new Properties();
        testResourceProps.put(JaxrsWhiteboardConstants.JAX_RS_RESOURCE, true);
        testResourceProps.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, appName);

        testConfiguration
                .add(createComponent().setInterface(Application.class.getName(), jaxRsApplicationProps).setImplementation(Application.class))
                .add(createComponent().setInterface(Object.class.getName(), testResourceProps).setImplementation(new TestResource()));
    }

    @After
    public void after() {
        cleanUp(this);
        Mockito.reset(m_mockPolicyProvider);
    }

    @Test
    public void testWithoutRole() {
        HttpRequest request;

        // User doesn't have the required role in any application
        when(m_appOneRoleProvider.getRoles(Mockito.any())).thenReturn(Collections.emptySet());
        when(m_appTwoRoleProvider.getRoles(Mockito.any())).thenReturn(Collections.emptySet());
        request = HttpRequest.get("http://localhost:8080/one/test");
        assertEquals(403, request.code());

        request = HttpRequest.get("http://localhost:8080/two/test");
        assertEquals(403, request.code());

        request = HttpRequest.get("http://localhost:8080/three/test");
        assertEquals(403, request.code()); // there is no RoleProvider for this app

        // User only has the required role in app one
        when(m_appOneRoleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("user"));
        when(m_appTwoRoleProvider.getRoles(Mockito.any())).thenReturn(Collections.emptySet());
        request = HttpRequest.get("http://localhost:8080/one/test");
        assertEquals(200, request.code());

        request = HttpRequest.get("http://localhost:8080/two/test");
        assertEquals(403, request.code());

        request = HttpRequest.get("http://localhost:8080/three/test");
        assertEquals(403, request.code()); // there is no RoleProvider for this app

        // User doesn't have the required role in app one and two
        when(m_appOneRoleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("user"));
        when(m_appTwoRoleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("user"));
        request = HttpRequest.get("http://localhost:8080/one/test");
        assertEquals(200, request.code());

        request = HttpRequest.get("http://localhost:8080/two/test");
        assertEquals(200, request.code());

        request = HttpRequest.get("http://localhost:8080/three/test");
        assertEquals(403, request.code()); // there is no RoleProvider for this app
    }

}
