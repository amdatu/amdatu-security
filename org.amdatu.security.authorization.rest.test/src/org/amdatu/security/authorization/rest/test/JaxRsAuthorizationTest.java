/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authorization.rest.test;

import com.github.kevinsawicki.http.HttpRequest;
import org.amdatu.security.authorization.AuthorizationPolicy;
import org.amdatu.security.authorization.AuthorizationPolicyProvider;
import org.amdatu.security.authorization.BreadAction;
import org.amdatu.security.authorization.annotation.ReadAction;
import org.amdatu.security.authorization.annotation.ResourceAttribute;
import org.amdatu.security.authorization.builder.AttributeQualifierBuilder;
import org.amdatu.security.authorization.builder.AuthorizationPolicyBuilder;
import org.amdatu.security.authorization.builder.AuthorizationRuleBuilder;
import org.amdatu.security.authorization.builder.EntityDescriptorBuilder;
import org.amdatu.security.authorization.rbac.RoleProvider;
import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.amdatu.web.testing.http.HttpTestConfigurator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.osgi.service.http.context.ServletContextHelper;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import javax.annotation.security.PermitAll;
import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Application;
import java.util.Collections;
import java.util.Optional;
import java.util.Properties;

import static org.amdatu.testing.configurator.TestConfigurator.*;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JaxRsAuthorizationTest  {

    private static final String TEST_SERVLET_CONTEXT_NAME = "org.amdatu.security.authorization.rest.test";
    private static final String TEST_JAX_RS_APP_NAME = "org.amdatu.security.authorization.rest.test";

    @RolesAllowed("user")
    @Path("test")
    public static class TestResource {

        @GET
        @Path("all")
        @PermitAll
        public String test() {
            return "hello";
        }

        @GET
        @Path("user")
        public String testUser() {
            return "hello";
        }

        @GET
        @Path("admin")
        @RolesAllowed("admin")
        public String testAdmin() {
            return "hello";
        }

        @GET
        @Path("admin/{userId}")
        @RolesAllowed("admin")
        @ReadAction("AdminOrCurrentUserResourceEntityType")
        public String testAdminOrCurrent(@PathParam("userId") @ResourceAttribute("userId") String userId) {
            return "hello";
        }

    }

    /**
     * ServletContextHelper implementation that put's the value of <code>Test-User-Id</code> in the
     * {@link ServletContextHelper#REMOTE_USER} attribute of the {@link HttpServletRequest}.
     *
     * Only for testing
     */
    private static final class TestServletContextHelper extends ServletContextHelper {
        @Override
        public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) {
            String testUserId = request.getHeader("Test-User-Id");
            if (testUserId != null) {
                request.setAttribute(ServletContextHelper.REMOTE_USER, testUserId);
            }
            return true;
        }
    }

    @Mock
    private RoleProvider m_roleProvider;

    @Mock
    private AuthorizationPolicyProvider m_mockPolicyProvider;

    @SuppressWarnings("unchecked")
    @Before
    public void before() {
        when(m_roleProvider.getRoles(Mockito.anyMap())).thenReturn(Collections.emptySet());
        when(m_roleProvider.getApplicationName()).thenReturn(TEST_JAX_RS_APP_NAME);

        Properties testServletContextProperties = new Properties();
        testServletContextProperties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_PATH, "/");
        testServletContextProperties.put(HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME, TEST_SERVLET_CONTEXT_NAME);

        Properties jaxRsApplicationProps = new Properties();
        jaxRsApplicationProps.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE, "/");
        jaxRsApplicationProps.put(JaxrsWhiteboardConstants.JAX_RS_NAME, TEST_JAX_RS_APP_NAME);
        jaxRsApplicationProps.put(AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT, TEST_SERVLET_CONTEXT_NAME);
        jaxRsApplicationProps.put(JaxrsWhiteboardConstants.JAX_RS_EXTENSION_SELECT, String.format("(%s=%s)", JaxrsWhiteboardConstants.JAX_RS_NAME, "amdatu-authorization"));
        jaxRsApplicationProps.put("amdatu-authorization", true);

        Properties testResourceProps = new Properties();
        testResourceProps.put(JaxrsWhiteboardConstants.JAX_RS_RESOURCE, true);
        testResourceProps.put(JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, TEST_JAX_RS_APP_NAME);


        configure(this)
            .add(createComponent().setInterface(ServletContextHelper.class.getName(), testServletContextProperties).setImplementation(new TestServletContextHelper()))
            .add(createComponent().setInterface(Application.class.getName(), jaxRsApplicationProps).setImplementation(Application.class))
            .add(createComponent().setInterface(RoleProvider.class.getName(), null).setImplementation(m_roleProvider))
            .add(createComponent().setInterface(AuthorizationPolicyProvider.class.getName(), null).setImplementation(m_mockPolicyProvider))
            .add(createComponent().setInterface(Object.class.getName(), testResourceProps).setImplementation(new TestResource()))
            .add(HttpTestConfigurator.createWaitForHttpEndpoints("http://localhost:8080/test/all"))
        .apply();
    }

    @After
    public void after() {
        cleanUp(this);
        Mockito.reset(m_mockPolicyProvider);
    }

    @Test
    public void testWithoutRole() {
        when(m_roleProvider.getRoles(Mockito.any())).thenReturn(Collections.emptySet());

        HttpRequest request;
        request = HttpRequest.get("http://localhost:8080/test/all");
        assertEquals(200, request.code());

        request = HttpRequest.get("http://localhost:8080/test/user");
        assertEquals(403, request.code());

        request = HttpRequest.get("http://localhost:8080/test/admin");
        assertEquals(403, request.code());
    }

    @Test
    public void testUnauthorizedRole() {
        when(m_roleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("guest"));

        HttpRequest request;
        request = HttpRequest.get("http://localhost:8080/test/all");
        assertEquals(200, request.code());

        request = HttpRequest.get("http://localhost:8080/test/user");
        assertEquals(403, request.code());

        request = HttpRequest.get("http://localhost:8080/test/admin");
        assertEquals(403, request.code());
    }

    @Test
    public void testAuthorizedRole() {

        when(m_roleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("user"));
        HttpRequest request;
        request = HttpRequest.get("http://localhost:8080/test/all");
        assertEquals(200, request.code());

        when(m_roleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("user"));
        request = HttpRequest.get("http://localhost:8080/test/user");
        assertEquals(200, request.code());

        when(m_roleProvider.getRoles(Mockito.any())).thenReturn(Collections.singleton("admin"));
        request = HttpRequest.get("http://localhost:8080/test/admin");
        assertEquals(200, request.code());
    }



    @Test
    public void testGetAdminOrCurrentUserWithCurrentUser() {

        AuthorizationPolicy authorizationPolicy = AuthorizationPolicyBuilder.build()
            .forActions(BreadAction.READ)
            .withSubjectDescriptor(EntityDescriptorBuilder.build()
                .withAttributeQualifiers(AttributeQualifierBuilder.build()
                    .withQualification(attrs -> attrs.containsKey(ServletContextHelper.REMOTE_USER))
                    .done())
                .done())
            .withResourceDescriptor(EntityDescriptorBuilder.build()
                .withEntityTypes("AdminOrCurrentUserResourceEntityType")
                .withAttributeQualifiers(AttributeQualifierBuilder.build()
                    .withQualification(attrs -> attrs.containsKey("userId"))
                    .done())
                .done())
            .withRules(AuthorizationRuleBuilder.build()
                .withAuthorization((sa, ra) -> {
                    if (sa.get(ServletContextHelper.REMOTE_USER).equals(ra.get("userId"))){
                        return Optional.of(true);
                    }
                    return Optional.empty();
                })
                .done())
            .done();

        HttpRequest request = HttpRequest.get("http://localhost:8080/test/admin/some-user-id")
                        .header("X-User", "some-user-id");
        Assert.assertEquals("Expect no access as the policy is not yet avaiable", 500, request.code());

        // "register the policy"
        Mockito.when(m_mockPolicyProvider.getPolicies()).thenReturn(Collections.singleton(authorizationPolicy));

        HttpRequest request2 = HttpRequest.get("http://localhost:8080/test/admin/some-user-id")
                        .header("Test-User-Id", "some-user-id");
        Assert.assertEquals("Expect access is now granted", 200, request2.code());
    }

    @Test
    public void testGetAdminOrCurrentUserWithUnauthorizedUser() {
        HttpRequest request = HttpRequest.get("http://localhost:8080/test/admin/some-user-id")
                        .header("Test-User-Id", "other-user-id");
        Assert.assertEquals(500, request.code());
    }

}
