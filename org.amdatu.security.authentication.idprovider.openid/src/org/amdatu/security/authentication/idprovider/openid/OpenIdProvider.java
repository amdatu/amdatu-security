/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider.openid;

import static org.amdatu.security.authentication.idprovider.IdProviderConstants.CODE_ACCESS_DENIED;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.ERROR_CODE;
import static org.amdatu.security.authentication.idprovider.IdProviderConstants.*;
import static org.amdatu.security.authentication.idprovider.openid.OpenIdProviderConfig.TYPE_OPENID;
import static org.amdatu.security.authentication.idprovider.openid.util.ConfigUtil.updateServiceProperties;
import static org.osgi.service.log.LogService.LOG_INFO;
import static org.osgi.service.log.LogService.LOG_WARNING;

import java.net.URI;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.amdatu.security.authentication.idprovider.IdProvider;
import org.apache.felix.dm.Component;
import org.openid4java.association.AssociationException;
import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.VerificationResult;
import org.openid4java.discovery.DiscoveryException;
import org.openid4java.discovery.Identifier;
import org.openid4java.message.MessageException;
import org.osgi.service.log.LogService;

/**
 * Provides an OpenID identity provider implementation.
 */
public class OpenIdProvider implements IdProvider {

    private static final String VERIFIED_ID = "verifiedId";

    // Injected by Felix DM...
    private volatile LogService m_log;
    private volatile Component m_component;
    // Locally managed...
    private volatile OpenIdProviderConfig m_config;

    /**
     * Creates a new {@link OpenIdProvider} instance.
     */
    public OpenIdProvider() {
        // Nop
    }

    @Override
    public URI getAuthenticationURI(URI callbackURI, Map<String, String[]> params) {
        try {
            return URI.create(m_config.createAuthURI(callbackURI));
        }
        catch (MessageException | ConsumerException | DiscoveryException e) {
            m_log.log(LOG_INFO, "Failed to get authentication URI!", e);

            throw new RuntimeException("Failed to get authentication URI!", e);
        }
    }

    @Override
    public String getType() {
        return TYPE_OPENID;
    }

    @Override
    public Map<String, String> getUserIdentity(URI callbackURI, Map<String, String[]> params) {
        OpenIdProviderConfig cfg = m_config;

        return getTokenAttributesFromRequest(cfg, callbackURI, params)
            .orElseGet(() -> createMapWithError(CODE_ACCESS_DENIED, cfg.getName()));
    }

    /**
     * Called by Felix DM.
     */
    protected final void start(Component comp) throws Exception {
        m_log.log(LOG_INFO, "OpenId ID provider started for type: " + m_config.getName());
    }

    /**
     * Called by Felix DM.
     */
    protected final void stop(Component comp) throws Exception {
        m_log.log(LOG_INFO, "OpenId ID provider stopped for type: " + m_config.getName());
    }

    /**
     * Called by Felix DM.
     */
    protected final void update(Dictionary<String, ?> config) throws Exception {
        OpenIdProviderConfig newConfig = null;
        if (config != null) {
            newConfig = new OpenIdProviderConfig(config);
        }
        m_config = newConfig;

        if (newConfig != null) {
            updateServiceProperties(m_component,
                PROVIDER_TYPE, TYPE_OPENID,
                PROVIDER_NAME, newConfig.getName());
        }
    }

    private Optional<Map<String, String>> getTokenAttributesFromRequest(OpenIdProviderConfig cfg,
        URI callbackURI, Map<String, String[]> params) {
        String providerName = cfg.getName();

        try {
            VerificationResult verificationResult = cfg.verifyResult(callbackURI, params);

            Identifier verifiedId = verificationResult.getVerifiedId();

            String error = verificationResult.getStatusMsg();
            if (error != null) {
                return Optional.of(createMapWithError(error, providerName));
            }

            Map<String, String> tokenAttrs = new HashMap<>();
            tokenAttrs.put(PROVIDER_TYPE, TYPE_OPENID);
            tokenAttrs.put(PROVIDER_NAME, providerName);
            tokenAttrs.put(VERIFIED_ID, verifiedId.getIdentifier());

            return Optional.of(tokenAttrs);
        }
        catch (DiscoveryException | MessageException | AssociationException e) {
            m_log.log(LOG_WARNING, "Failed to get user identity! Denying access!", e);

            return Optional.empty();
        }
    }

    private static Map<String, String> createMapWithError(String error, String name) {
        Map<String, String> tokenAttrs = new HashMap<>();
        tokenAttrs.put(PROVIDER_TYPE, TYPE_OPENID);
        tokenAttrs.put(PROVIDER_NAME, name);
        tokenAttrs.put(ERROR_CODE, error);
        return tokenAttrs;
    }
}
