/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.authentication.idprovider;

/**
 * Constants used by the {@link IdProvider}s.
 */
public interface IdProviderConstants {
    /**
     * The actual access token provided by the (external) ID provider.
     */
    String ACCESS_TOKEN = "accessToken";
    /**
     * The optional refresh token provided by the (external) ID provider.
     */
    String REFRESH_TOKEN = "refreshToken";
    /**
     * The optional identity token provided by the (external) ID provider (if it supports OpenID connect).
     */
    String ID_TOKEN = "id_token";
    /**
     * The local identifier provided by the local id provider.
     */
    String LOCAL_ID = "localId";
    /**
     * The error code/message that was returned by the (external) ID provider.
     */
    String ERROR_CODE = "error";
    /**
     * The mandatory type of a IdProvider, should be supplied as service property.
     */
    String PROVIDER_TYPE = "providerType";
    /**
     * The mandatory name of a IdProvider, should be supplied as service property.
     */
    String PROVIDER_NAME = "providerName";

    /**
     * Used by OAuth/OpenID connect to determine how the user should be authenticated.
     */
    String PROMPT = "prompt";
    /**
     * OpenID connect value for {@link #PROMPT}.
     */
    String PROMPT_NONE = "none";
    /**
     * OpenID connect value for {@link #PROMPT}.
     */
    String PROMPT_LOGIN = "login";

    /**
     * The user's credentials have been reset and need to be updated prior to continuing.
     */
    String CODE_CREDENTIALS_RESET = "credentials_reset";
    /**
     * The user has been successfully logged out.
     */
    String CODE_LOGGED_OUT = "logged_out";
    /**
     * The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.
     */
    String CODE_INVALID_REQUEST = "invalid_request";
    /**
     * The client is not authorized to request an authorization code using this method.
     */
    String CODE_UNAUTHORIZED_CLIENT = "unauthorized_client";
    /**
     * The resource owner or authorization server denied the request.
     */
    String CODE_ACCESS_DENIED = "access_denied";
    /**
     * The authorization server does not support obtaining an authorization code using this method.
     */
    String CODE_UNSUPPORTED_RESPONSE_TYPE = "unsupported_response_type";
    /**
     * The requested scope is invalid, unknown, or malformed.
     */
    String CODE_INVALID_SCOPE = "invalid_scope";
    /**
     * The authorization server encountered an unexpected condition that prevented it from fulfilling the request. (This error code is needed because a 500 Internal Server Error HTTP status code cannot be returned to the client via an HTTP redirect.)
     */
    String CODE_SERVER_ERROR = "server_error";
    /**
     * he authorization server is currently unable to handle the request due to a temporary overloading or maintenance of the server. (This error code is needed because a 503 Service Unavailable HTTP status code cannot be returned to the client via an HTTP
     * redirect.)
     */
    String CODE_TEMPORARILY_UNAVAILABLE = "temporarily_unavailable";
    /**
     * The Authorization Server requires End-User interaction of some form to proceed. This error MAY be returned when the prompt parameter value in the Authentication Request is none, but the Authentication Request cannot be completed without displaying a
     * user interface for End-User interaction.
     */
    String CODE_INTERACTION_REQUIRED = "interaction_required";
    /**
     * The Authorization Server requires End-User authentication. This error MAY be returned when the prompt parameter value in the Authentication Request is none, but the Authentication Request cannot be completed without displaying a user interface for
     * End-User authentication.
     */
    String CODE_LOGIN_REQUIRED = "login_required";
    /**
     * The End-User is REQUIRED to select a session at the Authorization Server. The End-User MAY be authenticated at the Authorization Server with different associated accounts, but the End-User did not select a session. This error MAY be returned when
     * the prompt parameter value in the Authentication Request is none, but the Authentication Request cannot be completed without displaying a user interface to prompt for a session to use.
     */
    String CODE_ACCOUNT_SELECTION_REQUIRED = "account_selection_required";
    /**
     * The Authorization Server requires End-User consent. This error MAY be returned when the prompt parameter value in the Authentication Request is none, but the Authentication Request cannot be completed without displaying a user interface for End-User
     * consent.
     */
    String CODE_CONSENT_REQUIRED = "consent_required";
    /**
     * The request_uri in the Authorization Request returns an error or contains invalid data.
     */
    String CODE_INVALID_REQUEST_URI = "invalid_request_uri";
    /**
     * The request parameter contains an invalid Request Object.
     */
    String CODE_INVALID_REQUEST_OBJECT = "invalid_request_object";
    /**
     * The OP does not support use of the request parameter.
     */
    String CODE_REQUEST_NOT_SUPPORTED = "request_not_supported";
    /**
     * The OP does not support use of the request_uri parameter.
     */
    String CODE_REQUEST_URI_NOT_SUPPORTED = "request_uri_not_supported";
    /**
     * The OP does not support use of the registration parameter.
     */
    String CODE_REGISTRATION_NOT_SUPPORTED = "registration_not_supported";
}
