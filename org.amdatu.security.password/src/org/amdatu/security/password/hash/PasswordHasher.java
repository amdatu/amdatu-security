/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.hash;

import org.osgi.annotation.versioning.ProviderType;

/**
 * Provides a secure way of hashing passwords.
 */
@ProviderType
public interface PasswordHasher {

    /**
     * Hashes the given password into a series of bytes in such way that it is computationally infeasible to
     * recover the original password from these bytes alone.
     *
     * @param input the password input to hash, cannot be <code>null</code>;
     * @return the hashed password, never <code>null</code>.
     */
    String hash(String input);

    /**
     * Determines whether the given input is already hashed or not.
     *
     * @param input the input to test, cannot be <code>null</code>.
     * @return <code>true</code> if the input is hashed, <code>false</code> otherwise.
     */
    boolean isHashed(String input);

    /**
     * Verifies whether two given strings match against each other.
     *
     * @param a the (hashed) string to verify against, cannot be <code>null</code>;
     * @param b the (hashed) string to verify, cannot be <code>null</code>.
     * @return true if the given strings match, <code>false</code> otherwise.
     */
    boolean verify(String a, String b);

}
