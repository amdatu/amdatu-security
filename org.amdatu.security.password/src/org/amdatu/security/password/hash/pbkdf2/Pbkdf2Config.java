/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.security.password.hash.pbkdf2;

import static org.amdatu.security.util.crypto.CryptoUtils.createSecureRandom;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Dictionary;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

import org.osgi.service.cm.ConfigurationException;

/**
 * Configuration for {@link Pbkdf2Hasher}.
 */
public class Pbkdf2Config {
    public static final String KEY_SALT_SIZE = "saltSize";
    public static final String KEY_HASH_SIZE = "hashSize";
    public static final String KEY_ITERATIONS = "iterations";
    public static final String KEY_PRNG_ALGORITHM = "secureRandomAlgorithm";

    private static final String PBKDF2_ALGORITHM = "PBKDF2WithHmacSHA1";
    private static final int PBKDF2_SALT_BYTE_SIZE = 24;
    private static final int PBKDF2_HASH_BYTE_SIZE = 18;
    private static final int PBKDF2_ITERATIONS = 64000;

    private final int m_saltSize;
    private final int m_hashSize;
    private final int m_iterations;
    private final SecureRandom m_prng;
    private final SecretKeyFactory m_secretKeyFactory;

    /**
     * Creates a new {@link Pbkdf2Config} instance.
     */
    public Pbkdf2Config() {
        m_saltSize = PBKDF2_SALT_BYTE_SIZE;
        m_hashSize = PBKDF2_HASH_BYTE_SIZE;
        m_iterations = PBKDF2_ITERATIONS;

        try {
            m_secretKeyFactory = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
            m_prng = createSecureRandom(null);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unable to obtain secret key factory or PRNG!", e);
        }
    }

    /**
     * Creates a new {@link Pbkdf2Config} instance.
     */
    public Pbkdf2Config(Dictionary<String, ?> config) throws ConfigurationException {
        m_saltSize = getInteger(config, KEY_SALT_SIZE, PBKDF2_SALT_BYTE_SIZE);
        m_hashSize = getInteger(config, KEY_HASH_SIZE, PBKDF2_HASH_BYTE_SIZE);
        m_iterations = getInteger(config, KEY_ITERATIONS, PBKDF2_ITERATIONS);
        String secureRandomAlg = getString(config, KEY_PRNG_ALGORITHM, null);

        try {
            m_secretKeyFactory = SecretKeyFactory.getInstance(PBKDF2_ALGORITHM);
            m_prng = createSecureRandom(secureRandomAlg);
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("Unable to obtain secret key factory or PRNG!", e);
        }
    }

    private static int getInteger(Dictionary<String, ?> dict, String key, int dflt) throws ConfigurationException {
        String val = (String) dict.get(key);
        if (val == null) {
            return dflt;
        }
        try {
            return Integer.parseInt(val);
        }
        catch (NumberFormatException e) {
            return dflt;
        }
    }

    private static String getString(Dictionary<String, ?> dict, String key, String dflt) throws ConfigurationException {
        String val = (String) dict.get(key);
        if (val == null || "".equals(val)) {
            return dflt;
        }
        return val;
    }

    /**
     * @return the hashed input using the given salt.
     */
    public byte[] generateHash(String input, byte[] salt) throws InvalidKeySpecException {
        return generateHash(input, salt, m_iterations, m_hashSize);
    }

    /**
     * @return the hashed input using the given salt, iteration count and key length.
     */
    public byte[] generateHash(String input, byte[] salt, int iterations, int keyLength)
        throws InvalidKeySpecException {
        PBEKeySpec keySpec = new PBEKeySpec(input.toCharArray(), salt, iterations, keyLength * 8);
        return m_secretKeyFactory.generateSecret(keySpec).getEncoded();
    }

    public byte[] generateSalt() {
        byte[] result = new byte[m_saltSize];
        m_prng.nextBytes(result);
        return result;
    }

    /**
     * @return the size of the total hash to generate, in bytes.
     */
    public int getHashSize() {
        return m_hashSize;
    }

    /**
     * @return the number of iterations to run the hashing algorithm.
     */
    public int getIterations() {
        return m_iterations;
    }

    /**
     * @return the random number generator, never <code>null</code>.
     */
    public SecureRandom getPRNG() {
        return m_prng;
    }

    /**
     * @return the size of the salt to generate, in bytes.
     */
    public int getSaltSize() {
        return m_saltSize;
    }

    /**
     * @return the secret key factory, never <code>null</code>.
     */
    public SecretKeyFactory getSecretKeyFactory() {
        return m_secretKeyFactory;
    }
}
